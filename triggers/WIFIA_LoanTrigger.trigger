trigger WIFIA_LoanTrigger on WIFIA_Loan__c (before update, before insert) {
    
    if (Trigger.isUpdate){
        WIFIA_LoanTriggerHandler.beforeUpdate(trigger.newMap, trigger.oldMap);
     }
    
    if (Trigger.isInsert){
        WIFIA_LoanTriggerHandler.beforeInsert(trigger.new);
     }
    
}