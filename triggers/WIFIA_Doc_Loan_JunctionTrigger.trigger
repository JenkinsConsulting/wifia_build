trigger WIFIA_Doc_Loan_JunctionTrigger on WIFIA_Document_Submission_Loan_Junction__c (after insert) {
    
    WIFIA_Doc_Loan_JunctionTriggerHandler.updateLoanDocSubDates(trigger.new);
}