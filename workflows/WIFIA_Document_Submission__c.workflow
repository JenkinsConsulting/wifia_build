<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>WIFIA_Document_Received_Notification_Alert</fullName>
        <description>WIFIA Document Received Notification Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/WIFIA_Document_Received_Notification</template>
    </alerts>
    <alerts>
        <fullName>WIFIA_Document_Submitted_Notification</fullName>
        <description>WIFIA Document Submitted Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>WIFIA_Knowledge_Management_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/WIFIA_Document_Submitted_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>WIFIA_Doc_Approval_Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>WIFIA Doc Approval Status: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIFIA_Doc_Approval_Status_Recalled</fullName>
        <field>Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>WIFIA Doc Approval Status: Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIFIA_Doc_Approval_Status_Recalledx</fullName>
        <field>Approval_Status__c</field>
        <formula>&quot;Recalled&quot;</formula>
        <name>WIFIA Doc Approval Status: Recalledx</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIFIA_Doc_Approval_Status_Under_Review</fullName>
        <field>Status__c</field>
        <literalValue>Under Review</literalValue>
        <name>WIFIA Doc Approval Status: Under Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIFIA_Doc_Approval_Status_Under_Reviewx</fullName>
        <field>Approval_Status__c</field>
        <formula>&quot;Under Review&quot;</formula>
        <name>WIFIA Doc Approval Status: Under Reviewx</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIFIA_Doc_Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>WIFIA Doc Status: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WIFIA_Doc_Status_Rejectedx</fullName>
        <field>Approval_Status__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>WIFIA Doc Status: Rejectedx</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <tasks>
        <fullName>Generates_Conditions_Precedent_to_Disbursement_Checklist</fullName>
        <assignedTo>jjenkins@innovateteam.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>Generate Conditions Precedent to Disbursement checklist.</description>
        <dueDateOffset>10</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Generates Conditions Precedent to Disbursement Checklist</subject>
    </tasks>
</Workflow>
