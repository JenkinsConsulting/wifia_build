<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>xWIFIA_Document_Submitted_Notification</fullName>
        <description>xWIFIA Document Submitted Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>WIFIA_Knowledge_Management_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/WIFIA_Document_Submitted_Notification</template>
    </alerts>
</Workflow>
