public without sharing class WIFIA_Doc_Loan_JunctionTriggerHandler {
     
     // This method is called by DocSubmissionAfterInsert trigger when new WIFIA_Document_Submission_Loan_Junction__c records are created
    public static void updateLoanDocSubDates(list<WIFIA_Document_Submission_Loan_Junction__c> DSLJs){
        system.debug('in updateLoanDocSubDates');
        
        list<id> loanIDs = new list<id>();
        list<WIFIA_Loan__c> loanMapList = new list<WIFIA_Loan__c>();
        
        for(WIFIA_Document_Submission_Loan_Junction__c dslj : DSLJs){
            loanIDs.add(dslj.WIFIA_Loan__c);
        }
        
        //get map of loans related to just created Document Submission records
        Map<id, WIFIA_Loan__c> loanMap = new Map<id, WIFIA_Loan__c>([select id from WIFIA_Loan__c where id in :loanIDs]);
        
        //get map of record type details (Custom Setting) 
        Map<string, RecordTypeDetails__c> getRTDmap = RecordTypeDetails__c.getAll();
        
        // create usable map of record type details (Custom Setting) where object = WIFIA_Document_Submission__c
        Map<string, RecordTypeDetails__c> RTDmap = new Map<string, RecordTypeDetails__c>();
        for(RecordTypeDetails__c rtd : getRTDmap.values()){
            if(rtd.Object_Name__c == 'WIFIA_Document_Submission__c' && rtd.Active__c){  
                RTDmap.put(rtd.Name, rtd);
            }
        }
        
        // update records using dynamic field name
        string fieldName; 
        for(WIFIA_Document_Submission_Loan_Junction__c dslj : DSLJs){
            fieldName = RTDmap.get(dslj.Document_Type__c).Received_Date_Field__c;
            loanMap.get(dslj.WIFIA_Loan__c).put(fieldName, system.today());
                  
        } // end for loop
        
        loanMapList.addAll(loanMap.values());
        update loanMapList;    
    }
}