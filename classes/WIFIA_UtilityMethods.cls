public class WIFIA_UtilityMethods {

    public static list<RecordTypeDetails__c> getRTDetails(){ 
        Map<string, RecordTypeDetails__c> RTDetailMap = RecordTypeDetails__c.getAll();
        List<RecordTypeDetails__c> RTDetails = new List<RecordTypeDetails__c>();
        RecordTypeDetails__c RTDetail = new RecordTypeDetails__c();
        
        for(String Name : RTDetailMap.keySet()){
            RTDetail = RTDetailMap.get(Name);
            if(RTDetail.Active__c){
                RTDetails.add(RTDetail);
            }
        }
       
        return RTDetails;
    }
    
    public static string getCurrentUserName(){
        id userID = UserInfo.getUserId();
        user u = [select id, FirstName, LastName from User where id = :userID limit 1];
        return u.FirstName + ' ' + u.LastName;
    }
    
}