public without sharing class WIFIA_LoanTriggerHandler {

    public static void beforeUpdate(map<id, WIFIA_Loan__c> updatedLoans, map<id, WIFIA_Loan__c> oldLoanMap){
        list<RecordTypeDetails__c> RTDetails = WIFIA_UtilityMethods.getRTDetails();
        string userName = WIFIA_UtilityMethods.getCurrentUserName();
        string todayDate = date.today().month()+'/'+date.today().day()+'/'+date.today().year();
        
        // variables used to loop through trigger provided maps
        WIFIA_Loan__c loan;
        WIFIA_Loan__c oldLoan;
        
        // variables used for calculating due dates
        decimal loanDocCount;
        date loanNextDue;
        date loanFirstDue;
        integer multiplier;
        
        // variables used to identify loan fields for each document record type,
        // provided in Record Type Details custom setting records (RTDetails)
        string WaivedFieldName;  
        string WaivedByFieldName;
        string FirstDueDateFieldName;
        string nextDueFieldName;
        string lastSubmitFieldName;
        string dueDateTimingFieldName;
        string docCountFieldName;
        
        
        
        
        for(Id loanID : updatedLoans.KeySet()){
            loan = updatedLoans.get(loanID);
            oldLoan = oldLoanMap.get(loanID);
            
            // loops through Doc Submission record types
            for(RecordTypeDetails__c RTDetail : RTDetails){
                
              // Updates Waived By fields if Waived field checked
                WaivedFieldName = RTDetail.Waived_Field__c;                
                WaivedByFieldName = RTDetail.Waived_By_Field__c;
                if((boolean)loan.get(WaivedFieldName) && !(boolean)oldLoan.get(WaivedFieldName)){
                    loan.put(WaivedByFieldName, userName+', '+todayDate);
                }
                
              // Updates Next Due Date if that field is null and 1st Due Date is populated
                FirstDueDateFieldName = RTDetail.First_Due_Date_Field__c;
                nextDueFieldName = RTDetail.Next_Due_Date_Field__c;
                dueDateTimingFieldName = RTDetail.Due_Date_Timing_Field__c;
                
                if((date)loan.get(FirstDueDateFieldName) != null &&
                   (date)loan.get(nextDueFieldName) == null &&
                   (string)loan.get(dueDateTimingFieldName) != 'NA' &&
                   (!(boolean)loan.get(WaivedFieldName))) 
                 {
                     loanFirstDue = (date)loan.get(FirstDueDateFieldName);    
                     loan.put(nextDueFieldName, loanFirstDue); 
                 }
                
              // Updates Next Due Date after document submission process updates a Last Submitted Date on the loan                 
                docCountFieldName = RTDetail.Doc_Count_Field__c;             
                loanDocCount = (decimal)loan.get(docCountFieldName);              
                loanNextDue = (date)loan.get(nextDueFieldName);           
                loanFirstDue = (date)loan.get(FirstDueDateFieldName);
                lastSubmitFieldName = RTDetail.Received_Date_Field__c;
                
                if((string)loan.get(dueDateTimingFieldName) != null &&
                   (date)loan.get(lastSubmitFieldName) != (date)oldLoan.get(lastSubmitFieldName) &&
                    (!(boolean)loan.get(WaivedFieldName)))
                {
                    switch on (string)loan.get(dueDateTimingFieldName) {
                        when 'Monthly' {multiplier = 1;}
                        when 'Quarterly' {multiplier = 3;}
                        when 'Semiannually' {multiplier = 6;}
                        when 'Annually' {multiplier = 12;}
                        when 'NA' {multiplier = 0;}
                    }
                    
                    if(multiplier != 0){
                         if((decimal)loan.get(docCountFieldName) == null){
                            loan.put(nextDueFieldName, loanFirstDue.addMonths(multiplier));
                            loan.put(docCountFieldName, 1);
                        }
                        else{
                            loan.put(nextDueFieldName, loanFirstDue.addMonths((integer.valueOf((decimal)loan.get(docCountFieldName))+1)*multiplier));
                            loan.put(docCountFieldName, loanDocCount + 1);
                        }
                    }
                    else {
                        loan.put(nextDueFieldName, null);
                        
                        if((decimal)loan.get(docCountFieldName) == null){
                            loan.put(docCountFieldName, 1);
                        }
                        else {
                            loan.put(docCountFieldName, loanDocCount + 1);
                        }
                    }    
                }    
                
            } // end Doc Submission record types loop                    
        } // end updated loans loop
    }
    
    public static void beforeInsert(list<WIFIA_Loan__c> insertedLoans){     
        list<RecordTypeDetails__c> RTDetails = WIFIA_UtilityMethods.getRTDetails();
        string userName = WIFIA_UtilityMethods.getCurrentUserName();
        string todayDate = date.today().month()+'/'+date.today().day()+'/'+date.today().year();
        string FirstDueDateFieldName;
        string nextDueFieldName;
        string WaivedFieldName;
        string WaivedByFieldName;
        date loanFirstDue;
        string dueDateTimingFieldName;

        for(WIFIA_Loan__c loan : insertedLoans){
            for(RecordTypeDetails__c RTDetail : RTDetails){
                
                // Updates Waived By fields if Waived field checked
                WaivedFieldName = RTDetail.Waived_Field__c;                
                WaivedByFieldName = RTDetail.Waived_By_Field__c;
                if((boolean)loan.get(WaivedFieldName)){
                    loan.put(WaivedByFieldName, userName+', '+todayDate);
                }
                
                // Updates Next Due Date if that field is null and 1st Due Date is populated
                FirstDueDateFieldName = RTDetail.First_Due_Date_Field__c;
                nextDueFieldName = RTDetail.Next_Due_Date_Field__c;
                dueDateTimingFieldName = RTDetail.Due_Date_Timing_Field__c;
                
                if((date)loan.get(FirstDueDateFieldName) != null &&
                   (date)loan.get(nextDueFieldName) == null &&
                   (string)loan.get(dueDateTimingFieldName) != 'NA' &&
                   (!(boolean)loan.get(WaivedFieldName))){
                       loanFirstDue = (date)loan.get(FirstDueDateFieldName);    
                       loan.put(nextDueFieldName, loanFirstDue); 
                }
                
            } // end Doc Submission record types loop                    
        } // end inserted loans loop
    }
    
}