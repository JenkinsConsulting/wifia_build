public class WIFIA_newLoanApex{
    //Controller class used by WIFIA_Loan_NEW Lightning Component
    
    @AuraEnabled
    public static List<string> MetadataLayoutSections(){
        List<string> secLabels = WIFIA_MetaDataMethods.getLayoutSectionLabels('WIFIA_Loan__c-WIFIA Loan Layout');
        
        return secLabels;    
    }
    
    @AuraEnabled
    public static List<WIFIA_MetadataMethods.LayoutItem> MetadataLayoutSampleAll(){
        
        List<WIFIA_MetadataMethods.LayoutItem> LIs = WIFIA_MetaDataMethods.getMetadataLayout('WIFIA_Loan__c-WIFIA Loan Layout');
        integer columnCount = 1;
        for (WIFIA_MetadataMethods.LayoutItem li : LIs){
            if (li.columnNum == 1){
                columnCount = 2;
                break;
            }
        }

        return LIs;
    }
    
    @AuraEnabled
    public static List<WIFIA_MetadataMethods.LayoutItem> MetadataLayoutSample(Integer sectionNum){
        
        List<WIFIA_MetadataMethods.LayoutItem> LIs = WIFIA_MetaDataMethods.getMetadataLayout('WIFIA_Loan__c-WIFIA Loan Layout', sectionNum);
        integer columnCount = 1;
        for (WIFIA_MetadataMethods.LayoutItem li : LIs){
            if (li.columnNum == 1){
                columnCount = 2;
                break;
            }
        }
        system.debug('LIs: '+LIs);
        system.debug('LIs length: '+LIs.size());
        return LIs;
    }   
    
    @AuraEnabled
      //This action is initiated after document uploaded for user to select related loan record(s)
    public static list<WIFIA_Loan__c> getUserLoans(string uID){
        // get RecordTypeDetails custom setting records
        list<RecordTypeDetails__c> RTDetails = WIFIA_UtilityMethods.getRTDetails();
        
        string queryFields;
        for(RecordTypeDetails__c RTDetail : RTDetails){
            if(queryFields == null){
                queryFields = RTDetail.Next_Due_Date_Field__c+
                              ', '+RTDetail.Received_Date_Field__c+
                              ', '+RTDetail.Waived_Field__c;
            }
            else {
                queryFields = queryFields+', '+RTDetail.Next_Due_Date_Field__c+
                              ', '+RTDetail.Received_Date_Field__c+
                              ', '+RTDetail.Waived_Field__c;    
            }                  
        }

        list<WIFIA_Loan__c> allLoans = Database.query('Select id, name, '+queryFields+' from WIFIA_Loan__c where ownerid = :uID');
        
        return allLoans;
    }
    
    @AuraEnabled
      //This action is initiated when component saves loan record
    public static string saveLoan(string LoanID){
        string message = 'Success';
        
        if(LoanID == 'new'){
            
        }
        
        else {
        }
        
        return message;
    }
    
    
    
}