public class WIFIA_AmortToolApex{
    
    public string loanName {get; set;}
    public date closeDate {get; set;}
    public decimal loanAmount {get; set;}
    public decimal intRate {get; set;}
    public string walYM {get; set;}
    public date matDate {get;set;}
    public list<exhibitF> exhibitData {get; set;}
    public decimal disbursInPeriodTotal {get;set;}
    public decimal capInterestTotal {get;set;}
    public decimal intPaymentTotal {get;set;}
	public decimal prinRepaymentTotal {get;set;}
   	public decimal debtServPaymentTotal {get;set;}	
		
    public WIFIA_AmortToolApex() {
    	
    	string loanID = System.currentPageReference().getParameters().get('loanID');
    	walYM = System.currentPageReference().getParameters().get('walYM');
    	WIFIA_Loan__c loan = [select id, Most_Recent_Amort_Calcs_JSON__c, Loan_Close_Date__c, Name, WIFIA_Interest_Rate__c, Final_Maturity_Date__c, Loan_Amount__c from WIFIA_Loan__c where id = :loanID limit 1];
        
        loanName = loan.Name;
        closeDate = loan.Loan_Close_Date__c;
        loanAmount = loan.Loan_Amount__c;
        intRate = loan.WIFIA_Interest_Rate__c;
        matDate = loan.Final_Maturity_Date__c;
        
        //get json for exhibit f
        exhibitData = (List<exhibitF>)System.JSON.deserializeStrict(loan.Most_Recent_Amort_Calcs_JSON__c, List<exhibitF>.Class);
        
        //calculate totals
        decimal disbursInPeriodct = 0;
        decimal capInterestct = 0;
        decimal intPaymentct = 0;
        decimal prinRepaymentct = 0;
        decimal debtServPaymentct = 0;
        
        for(exhibitF xitem : exhibitData){
        	disbursInPeriodct = disbursInPeriodct + xitem.disbursInPeriod;
        	capInterestct = capInterestct + xitem.capInterest;
        	intPaymentct = intPaymentct + xitem.intPayment;
        	prinRepaymentct = prinRepaymentct + xitem.prinRepayment;
        	debtServPaymentct = debtServPaymentct + xitem.debtServPayment;
        }
        
        disbursInPeriodTotal = disbursInPeriodct;
        capInterestTotal = capInterestct;
        intPaymentTotal = intPaymentct;
        prinRepaymentTotal = prinRepaymentct;
        debtServPaymentTotal = debtServPaymentct;
        
    }
    
    @AuraEnabled
    public static string saveJson(id loanID, string sculptRecords, string drawRecords){
    	
    	WIFIA_Loan__c theloan = [select id, Loan_Sculpting_JSON__c, Loan_Draws_JSON__c from WIFIA_Loan__c where id = :loanID limit 1];
    	theloan.Loan_Sculpting_JSON__c = sculptRecords;
    	theloan.Loan_Draws_JSON__c = drawRecords;
    	
    	string msg = 'success!';
    	
    	try{
    		update theloan;
    		}
    	catch(exception e){
            system.debug('error: '+e);
            msg = 'error: '+e;
        }	
    	    	
    	return msg;
    }
    
    @AuraEnabled
    public static WIFIA_Loan__c getLoanInfo(id loanID){
        
        WIFIA_Loan__c loan = [select id, Amort_Redirect__c, Name, Repayment_Schedule_Day__c, Repayment_Schedule_Months__c, Borrower_State__c, Project_Name__c, Borrower_Name__r.Name, WIFIA_Interest_Rate__c, Final_Maturity_Date__c, Loan_Amount__c, Loan_Close_Date__c from WIFIA_Loan__c where id = :loanID limit 1];
        
        return loan;
    }
    
    @AuraEnabled
    public static string saveLoan(string LoanID, string saveAction){
        string message = 'Success';
        
        wifia_loan__c theLoan = [select id from wifia_loan__c where id = :LoanID limit 1];
        
        switch on saveAction {
            when 'set amort'{       
                theLoan.Amort_Redirect__c = true;
                system.debug('setting redirect to true');
            }   
            when 'clear amort'{       
                theLoan.Amort_Redirect__c = false;
                system.debug('setting redirect to false');
                system.debug('for loan: '+theLoan.id);
            }
            when else {  
                system.debug('no action taken');
            }
        }    
            
        try{
        	update theLoan;
        }
        catch(exception e){
        	message = 'error: ' + e;
        	system.debug(message);
        }
        system.debug(message);
        return message;
    }
    
    @AuraEnabled
    public static list<WIFIA_Disbursement__c> getDisbursements(id loanID){
        
        list<WIFIA_Disbursement__c> disbursements = [select id, Executed_Draw_Date__c, Executed_Draw_Amount__c  
                                from WIFIA_Disbursement__c where WIFIA_Loan__c = :loanID order by Executed_Draw_Date__c];
        
        return disbursements;
    }
    
    @AuraEnabled
    public static string createNewDisbursement(string loanID, date drawDate, decimal drawAmt){
      
      	WIFIA_Disbursement__c newDisbursement = new WIFIA_Disbursement__c(name='New Payment', WIFIA_Loan__c=loanID, Executed_Draw_Date__c=drawDate, Executed_Draw_Amount__c=drawAmt);
        insert newDisbursement;
        
        string newID = string.valueOf(newDisbursement.id);
        
        return newID;
    }
    
    @AuraEnabled
    public static string deleteNewDisbursement(string drawID){
        
        string confirm;
        
        try{
            delete new WIFIA_Disbursement__c(id = drawID);
            confirm = 'deleted';
        }
        catch(exception e){
            system.debug('error: '+e);
            confirm = 'error: '+e;
        }
        
        return confirm;
    }
    
    @AuraEnabled
    public static string getLoanSculptingJSON(id loanID){
        
        string loanSculptingJSON = [select id, Loan_Sculpting_JSON__c from WIFIA_Loan__c where id = :loanID limit 1].Loan_Sculpting_JSON__c;
        
        return loanSculptingJSON;
    }
    
    @AuraEnabled
    public static string getLoanDrawJSON(id loanID){
        
        string loanDrawJSON = [select id, Loan_Draws_JSON__c from WIFIA_Loan__c where id = :loanID limit 1].Loan_Draws_JSON__c;
        
        return loanDrawJSON;
    }
    
    @AuraEnabled
    public static list<string> amortCalc(string loanID, string sculptString, string drawString, decimal intRate, string closeDate, string purpose){
		
		intRate = intRate / 100;  // set decimal point correctly
		system.debug('intRate: '+intRate);
		
    	// loan scupting table
		sculptItem[] lstSculptItems = (List<sculptItem>)System.JSON.deserializeStrict(sculptString, List<sculptItem>.Class);
		system.debug('lstSculptItems: '+lstSculptItems);
		
		// draw table
		drawItem[] lstDrawItems = (List<drawItem>)System.JSON.deserialize(drawString, List<drawItem>.Class);
		
		integer paymentDateCt = lstSculptItems.size();
		
		date lastDrawDate;
		string dateRecdStr;
		date dateRecd;
		integer lastDrawPeriod = 0;
		
		if(lstDrawItems.size() > 0){
		//find last draw date and last draw period
			for(drawItem di : lstDrawItems){
				dateRecdStr = di.drawDate;
				dateRecdStr = dateRecdStr.mid(5,2)+'/'+dateRecdStr.right(2)+'/'+dateRecdStr.left(4);
				dateRecd = date.parse(dateRecdStr);
				if(dateRecd > lastDrawDate || lastDrawDate == null){
					lastDrawDate = dateRecd;
				}
			}
			for(integer x = 0; x < paymentDateCt; x++){
				if(x==0){
					lastDrawPeriod=1;
				}
				else{	
					if(date.parse(lstSculptItems[x].periodEnd)<lastDrawDate){
						lastDrawPeriod=x+2;
					}
				}
			}
		}
		
		integer levelPaymentCt = 0;
		integer levelPaymentAnnualCt = 0;
		integer levelPaymentOnePerYrCt = 0;
		for(sculptItem si : lstSculptItems){
			if(si.periodRepaymentType == 'Level Payment (semiannual)'){
				levelPaymentCt++;
			}
			if(si.periodRepaymentType == 'Level Payment (annual p, semiannual i)'){
				levelPaymentAnnualCt++;
			}
			if(si.periodRepaymentType == 'Level Payment (one payment per year)'){
				levelPaymentOnePerYrCt++;
			}
		}
		system.debug('levelPaymentCt: '+levelPaymentCt);
		
		
		
	///// ****** build principal and interest calculations tables ******* ///////////
		
		// interest calc variables
		decimal iafpd;
		decimal iafpdDec;
		decimal iafpdCalc;
		decimal iafp;
		decimal iafpDec;
		decimal iafpCalc;
		decimal tia;
		decimal ip;
		string formatDateReceived;
		date periodStart;
		date periodEnd;
		string formatCloseDate = closeDate.mid(5,2)+'/'+closeDate.right(2)+'/'+closeDate.left(4);
		date drawDate;
		decimal dayCount;
		double monthCount;
		
		// principal calc variables
		decimal ob;
		decimal maxOB = 0;
		decimal dip;
		decimal LV;
		decimal pmt;
		double LVSr = intRate / 2;
		double LVAr = intRate;
		double LVOr = double.valueOf(decimal.valueof((math.pow((1+(double.valueof(intRate)/2)),2))-1).setscale(8)); // effective interest rate
//		decimal nper = levelPaymentCt;
		decimal LVSpv = 0;
		decimal LVApv = 0;
		decimal LVOpv = 0;
		integer LVSstart;
		integer LVAstart;
		integer LVOstart;
		decimal ipmt;
		decimal trip;
		decimal rip;
		decimal ci;
		decimal oe;
		decimal maxOE = 0;
		decimal sumRIP = 0;
		date firstDraw = date.parse('1/1/2200');
		date dueDate;
		decimal sumProdCalc = 0;
		decimal sumProd = 0;
		decimal walYearCalc = 0;
		string walYears;
		string walYearsMonths;
		string numYears;
		decimal walYear2Calc = 0;
		string walYears2;
		
		list<interestCalculation> interestCalcList = new list<interestCalculation>();
		list<principalCalculation> principalCalcList = new list<principalCalculation>();
		list<string> validationList = new list<string>();
		string errorCheck = 'none';
		
		for(integer i = 0; i < paymentDateCt; i++){
		
		///// ****** interest calculations table ******* ///////////
			
		 // ** get Interest Accrued From Draws in Period **
		 	// get draws in this period
		 	periodStart = i==0 ? date.parse(formatCloseDate) : (date.parse(lstSculptItems[i-1].periodEnd))+1;
			periodEnd = date.parse(lstSculptItems[i].periodEnd); 
			iafpd = 0;
			dip = 0; // parameter for principal table
		 	for(drawItem di : lstDrawItems){
		 		formatDateReceived = di.drawDate;
				formatDateReceived = formatDateReceived.mid(5,2)+'/'+formatDateReceived.right(2)+'/'+formatDateReceived.left(4);
				drawDate = date.parse(formatDateReceived);
				if(firstDraw > drawDate){firstDraw = drawDate;}
				if(periodStart <= drawDate && drawDate <= periodEnd){
					dayCount = drawDate.daysBetween(periodEnd);
					monthCount = ((dayCount / 15).setScale(0))/2;
					if(purpose == 'Loan Management (post close)'){
						iafpdCalc = di.executedAmount * monthCount * intRate / 12;
					}
					else {
						iafpdCalc = (di.executedAmount + di.anticipatedAmount) * monthCount * intRate / 12;
					}
					//iafpdCalc = di.Payment_Amount * monthCount * intRate / 12;
					iafpd = iafpd + iafpdCalc.setScale(2, RoundingMode.CEILING);

					if(purpose == 'Loan Management (post close)'){
						dip = dip + di.executedAmount;
					}
					else {
						dip = dip + (di.executedAmount + di.anticipatedAmount);
					}		
				}
		 	}				
		
		 // ** get Interest Accrued From Principal **		
		 	if(i==0){
		 		iafp = 0;
		 	}
		 	else {
		 		iafpCalc = principalCalcList[i-1].outstandingEnd * intRate / 2;
		 		iafp = iafpCalc.setScale(2, RoundingMode.CEILING);
		 		
		 	}
		 	
		 // ** get Total Interest Accrued **		
		 	tia = iafpd + iafp;
		
		 // ** get Interest Payment **	
		 	switch on lstSculptItems[i].periodRepaymentType {
				when 'No Payment' {
					ip = 0;
				}
				when 'Partial Payment P&I ($)' {
					ip = lstSculptItems[i].intDoll;
				}
				when 'Partial Payment P&I (%)' {
					ip = (lstSculptItems[i].intPerc / 100) * tia;
					ip = ip.setScale(2);
				}
		 		when else {
		 			ip = tia;
		 		}
		 	}

			interestCalcList.add(new interestCalculation(iafpd, iafp, tia, ip));
		
	///// ****** interest calculations table end ******* ///////////
	
	///// ****** principal calculations table ******* ///////////
			
		 // ** get outstanding beginning **
			ob = i==0 ? 0 : principalCalcList[i-1].outstandingEnd;
			if(maxOb < ob){maxOb = ob;}
			
		 // ** get draws in this period **
		 	// dip calculated in interest table draw loop
			
		 // ** get Temp Repayment in Period **
			switch on lstSculptItems[i].periodRepaymentType {
				when 'Level Payment (semiannual)' {
					if(LVSpv==0){
						LVSpv = ob * -1;
						LVSstart = i - 1;
					}
					
					pmt = LVSr / (Math.pow(1 + LVSr, levelPaymentCt) - 1) * -(LVSpv * Math.pow(1 + LVSr, levelPaymentCt));
					
					ipmt = (LVSpv*LVSr*((math.pow((LVSr + 1),(levelPaymentCt + 1))) - (math.pow((LVSr + 1),(i - LVSstart))))) / ((LVSr + 1) * ((math.pow((LVSr + 1),levelPaymentCt)) - 1));
					ipmt = ipmt * -1;	    
				
					LV = pmt - ipmt;  // aka ppmt
					LV = LV.setScale(2, RoundingMode.CEILING);
					
				}
				when 'Level Payment (annual p, semiannual i)' {
					if(LVApv==0){
						LVApv = ob * -1;
						LVAstart = i;
						
					}
					pmt = LVAr / (Math.pow(1 + LVAr, levelPaymentAnnualCt) - 1) * -(LVApv * Math.pow(1 + LVAr, levelPaymentAnnualCt));
		
					ipmt = (LVApv*LVAr*((math.pow((LVAr + 1),(levelPaymentAnnualCt + 1))) - (math.pow((LVAr + 1),((i - LVAstart)/2+1))))) / ((LVAr + 1) * ((math.pow((LVAr + 1),levelPaymentAnnualCt)) - 1));
					ipmt = ipmt * -1;
				
					LV = pmt - ipmt;  // aka ppmt
					LV = LV.setScale(2, RoundingMode.CEILING);	
				}
				when 'Level Payment (one payment per year)' {
					if(LVOpv==0){
						LVOpv = principalCalcList[i-1].outstandingBeginning;
					}
	
					pmt = LVOr / (Math.pow(1 + LVOr, levelPaymentOnePerYrCt) - 1) * -(LVOpv * Math.pow(1 + LVOr, levelPaymentOnePerYrCt));
					pmt = pmt * -1;
			
					LV = pmt - (ob*LVAr/2);
					LV = LV.setScale(2, RoundingMode.CEILING);
				}
				when 'Fixed Principal ($)' {
					LV = lstSculptItems[i].prinDoll;
				}
				when 'Fixed Principal (%)' {
					LV = (lstSculptItems[i].prinPerc / 100) * maxOb;
					LV = LV.setScale(2, RoundingMode.CEILING);
				}
				when 'Partial Payment P&I ($)' {
					LV = lstSculptItems[i].prinDoll;
				}
				when 'Partial Payment P&I (%)' {
					LV = (lstSculptItems[i].prinPerc / 100) * maxOb; 
					LV = LV.setScale(2, RoundingMode.CEILING);
				}
				when else {
					LV = 0;
				}			
			}  // end switch
			
			trip = LV;
			
		 // ** get Repayment in Period **
			rip = i==(paymentDateCt-1) ? ob : trip;
			
			//calculate sumProd for Weighted-Average Life (WAL) from first disbursement (years)
			if(rip != 0){
				dueDate = date.parse(lstSculptItems[i].periodEnd);
				dueDate = dueDate + 1;
				
				sumProdCalc = firstDraw.monthsBetween(dueDate); 
				
				sumProdCalc = sumProdCalc * 30; 
			
				if(dueDate.day() != firstDraw.day()){
					sumProdCalc = sumProdCalc + (dueDate.day() - firstDraw.day()); 
				}		
				sumProdCalc = sumProdCalc / 360; 
				sumProdCalc = sumProdCalc * rip; 
				
			}
			else{sumProdCalc = 0;}
			sumProd = sumProd + sumProdCalc; 
			
			//calculate Weighted-Average Life (WAL) from first period (years)
			walYear2Calc = walYear2Calc + (rip * (i+1));
		
		 // ** get Capitalized Interest **			

		 	ci = tia - ip;
		 	
		 	
		 // ** get Outstanding (end) **	
		 	oe = ob + dip - rip + ci;
		 	if(maxOE < oe){maxOE = oe;}
		 	sumRIP = sumRIP + rip;
			principalCalcList.add(new principalCalculation(ob, dip, trip, rip, ci, oe));
			
		///// ****** principal calculations table end ******* ///////////	
		
		//// validations
		string msg = '';
		if((lstSculptItems[i].periodRepaymentType=='Fixed Principal ($)' || lstSculptItems[i].periodRepaymentType=='Partial Payment P&I ($)') && lstSculptItems[i].prinDoll > ob){
			errorCheck='error';
			msg = 'Error: Too much principal';
		}
		if((lstSculptItems[i].periodRepaymentType=='Fixed Principal ($)' || lstSculptItems[i].periodRepaymentType=='Partial Payment P&I ($)') && lstSculptItems[i].intDoll > tia){
			errorCheck='error';
			if(msg != ''){
				msg = 'Error: Too much interest';
			}
			else{msg = ' / Error: Too much interest';}
		}
		if(i+1 <= lastDrawPeriod && lstSculptItems[i].periodRepaymentType!='No Payment' && lstSculptItems[i].periodRepaymentType!='Interest Only Total'){
			errorCheck='error';
			if(msg == ''){
				msg = 'Error: period must be no payment or Cap I';
			}
			else{msg = ' / Error: period must be no payment or Cap I';}
		}
		
		validationList.add(msg);
	}
		
	///// ****** output one table ******* ///////////
		
		// build output1 table
		date payDate;
		decimal principal = 0;
		decimal intCalc = 0;
		decimal totalPmt = 0;
		
		list<outputOne> outputOneList = new list<outputOne>();
		for(integer i = 0; i < paymentDateCt; i++){
			payDate = date.parse(lstSculptItems[i].periodEnd); 
			payDate = payDate + 1;
			//system.debug('payDate: '+payDate);
			
			principal = principalCalcList[i].repaymentInPeriod;
			//system.debug('principal(repaymentInPeriod): '+principal);
			
			intCalc = interestCalcList[i].interestPayment;
			//system.debug('interest(interestPayment): '+intCalc);
			
			totalPmt = principal + intCalc;
			//system.debug('totalPmt: '+totalPmt);
			
			outputOneList.add(new outputOne(payDate, principal, intCalc, totalPmt));
		}
		system.debug('outputOneList: '+outputOneList);
		
	///// ****** output one table end ******* ///////////	


	///// ****** ehibit F output table ******* ///////////
		date f_psd;
		date f_ped;
		date f_ppd;
		decimal f_dip;
		decimal f_ci;
		decimal f_ip;
		decimal f_pr;
		decimal f_mpo;
		decimal f_dsp;
		decimal f_eb;
		
		list<exhibitF> exhibitFList = new list<exhibitF>();
		for(integer i = 0; i < paymentDateCt; i++){
			f_psd = i==0 ? date.parse(formatCloseDate) : exhibitFList[i-1].perEndDate + 1;
			f_ped = date.parse(lstSculptItems[i].periodEnd); 
			f_ppd = f_ped + 1;
			f_dip = principalCalcList[i].drawsInPeriod;
			f_ci = principalCalcList[i].capitalizedInterest;
			f_ip = interestCalcList[i].interestPayment;
			f_pr = principalCalcList[i].repaymentInPeriod;
			f_mpo = lstSculptItems[i].prinPerc;
			f_dsp = outputOneList[i].totalPayment;
			f_eb = principalCalcList[i].outstandingEnd;
			
			exhibitFList.add(new exhibitF(f_psd, f_ped, f_ppd, f_dip, f_ci, f_ip, f_pr, f_mpo, f_dsp, f_eb));
		//	system.debug('f list: '+exhibitFList[i]);		
		}
		
		WIFIA_loan__c theLoan = [select id from WIFIA_loan__c where id = :loanID limit 1];
		theLoan.Most_Recent_Amort_Calcs_JSON__c = JSON.serialize(exhibitFList);
		update theLoan;
		
		
	///// ****** ehibit F table end ******* ///////////		
		
	///// *** final calculation and return string list build *** //////////////
		if(sumProd != 0 && sumRIP !=0){
			walYearCalc = (sumProd / sumRIP).setScale(1);
			walYears = string.valueOf(walYearCalc);
			walYearsMonths = string.valueOf(walYearCalc.setScale(0, RoundingMode.DOWN) + '-' + ((12*(walYearCalc - (walYearCalc.setScale(0, RoundingMode.DOWN)))).setScale(0))); 
			walYears2 = string.valueOf((walYear2Calc / maxOE / 2).setScale(1));
		}
		else{
			walYears = '';
			WalYearsMonths = '';
		}
		
		numYears = string.valueOf(lstSculptItems.size());
		
		list<string> retStrings = new list<string>();
		retStrings.add(JSON.serialize(outputOneList));
		//retStrings.add(JSON.serialize(outputTwoList));
		retStrings.add(string.valueOf(maxOE));
		retStrings.add(string.valueOf(sumRIP));
		retStrings.add(walYears);
		retStrings.add(walYearsMonths);
		retStrings.add(numYears);
		retStrings.add(errorCheck);
		retStrings.add(JSON.serialize(validationList));
		
		return retStrings;
    }
    
    public class exhibitF {
    	public date perStartDate {get;set;}
	    public date perEndDate {get;set;}
		public date perPmtDate {get;set;}
		public decimal disbursInPeriod {get;set;}
		public decimal capInterest {get;set;}
		public decimal intPayment {get;set;}
		public decimal prinRepayment {get;set;}
		public decimal maxPrinOutstanding {get;set;}
		public decimal debtServPayment {get;set;}
		public decimal endBalance {get;set;}
		
		public exhibitF(date fpsd, date fped, date fppd, decimal fdip, decimal fci, decimal fip, decimal fpr, decimal fmpo, decimal fdsp, decimal feb){
			perStartDate = fpsd;
		    perEndDate = fped;
			perPmtDate = fppd;
			disbursInPeriod = fdip;
			capInterest = fci;
			intPayment = fip;
			prinRepayment = fpr;
			maxPrinOutstanding = fmpo; 
			debtServPayment = fdsp;
			endBalance = feb;
		}
    }
    	
    public class interestCalculation { 
        @AuraEnabled
        public double interestAccruedFromPeriodDraw {get;set;}
        
        @AuraEnabled
        public double interestAccruedFromPrincipal {get;set;}
        
        @AuraEnabled
        public double totalInterestAccrued {get;set;}
        
        @AuraEnabled
        public double interestPayment {get;set;}
        
        public interestCalculation(decimal iafpd, decimal iafp, decimal tia, decimal ip) { 
        	interestAccruedFromPeriodDraw = iafpd;
			interestAccruedFromPrincipal = iafp;
			totalInterestAccrued = tia;
			interestPayment = ip;
    	}
    }
    
    public class principalCalculation { 
        @AuraEnabled
        public double outstandingBeginning {get;set;}
        
        @AuraEnabled
        public double drawsInPeriod {get;set;}
        
        @AuraEnabled
        public double tempRepaymentInPeriod {get;set;}
        
        @AuraEnabled
        public double repaymentInPeriod {get;set;}
        
        @AuraEnabled
        public double capitalizedInterest {get;set;}
        
        @AuraEnabled
        public double outstandingEnd {get;set;}
        
        public principalCalculation(decimal ob, decimal dip, decimal trip, decimal rip, decimal ci, decimal oe) { 
        	outstandingBeginning = ob;
        	drawsInPeriod = dip;
        	tempRepaymentInPeriod = trip;
        	repaymentInPeriod = rip;
        	capitalizedInterest = ci;
        	outstandingEnd = oe;
    	}
    }
    
    public class outputOne { 
        @AuraEnabled
        public date paymentDate {get;set;}
        
        @AuraEnabled
        public decimal principal {get;set;}
        
        @AuraEnabled
        public decimal interest {get;set;}
        
        @AuraEnabled
        public decimal totalPayment {get;set;}
        
        public outputOne(date pd, decimal p, decimal intCalc, decimal tp) { 
        	paymentDate = pd;	
        	principal = p;	
        	interest = intCalc;
        	totalPayment = tp;
    	}
    }
    
    public class drawItem { 
        @AuraEnabled
        public string indexVar {get;set;}
        
        @AuraEnabled
        public string drawDate {get;set;}
        
        @AuraEnabled
        public double anticipatedAmount {get;set;}
        
        @AuraEnabled
        public double executedAmount {get;set;}
        
        public drawItem(string iv, string dr, string aa, string ea) { 
        	indexVar = iv;
        	drawDate = dr;
        	anticipatedAmount = decimal.valueOf(aa);
        	executedAmount = decimal.valueOf(ea);	
    	}
    }
    
    public class sculptItem { 
        @AuraEnabled
        public string indexVar {get;set;}
        
        @AuraEnabled
        public string periodEnd {get;set;}
        
        @AuraEnabled
        public string periodRepaymentType {get;set;}
        
        @AuraEnabled
        public decimal prinDoll {get;set;}
        
        @AuraEnabled
        public decimal prinPerc {get;set;}
        
        @AuraEnabled
        public decimal intDoll {get;set;}
        
        @AuraEnabled
        public decimal intPerc {get;set;}
        
        @AuraEnabled
        public string message {get;set;}
        
        public sculptItem(string iv, string pe, string prt, decimal pd, decimal pp, decimal intD, decimal intP) { 
        	indexVar = iv;
        	periodEnd = pe;
        	periodRepaymentType = prt;        	
        	prinDoll = pd;
        	prinPerc = pp;
        	intDoll = intD;
        	intPerc = intP;
    	}
    }
      
}