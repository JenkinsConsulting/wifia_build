public class WIFIA_DocumentSubmissionApex{
    //Controller class used by WIFIA_DocumentSubmission Lightning Component and WIFIA_DocumentSubmission_VF visualforce page
    public string userName {get;set;}
    
    // used by VF page
    public WIFIA_DocumentSubmissionApex() {
        id userID = UserInfo.getUserId();
        user u = [select id, FirstName, LastName from User where id = :userID limit 1];
        userName = u.FirstName + ' ' + u.LastName;
    }
    
    @AuraEnabled
      //This action is used by Lightning Component to get user profile info
    public static string getUserProfile(string uID){
        user curUser = [select id, profileid from user where id = :uID limit 1];
        string profileName = [select id, name from profile where id = :curUser.profileid limit 1].name;
        
        return profileName;
    }
    
    @AuraEnabled
      //This action is initiated after document uploaded for user to select related loan record(s)
    public static list<WIFIA_Loan__c> getLoans(string uID, string borrowerName, boolean isCommunityUser){
        // get RecordTypeDetails custom setting records
        list<RecordTypeDetails__c> RTDetails = WIFIA_UtilityMethods.getRTDetails();
        system.debug('RTDetails: ' + RTDetails);
        string queryFields;
        for(RecordTypeDetails__c RTDetail : RTDetails){

            if(queryFields == null){
                queryFields = RTDetail.Next_Due_Date_Field__c+
                              ', '+RTDetail.Received_Date_Field__c+
                              ', '+RTDetail.Waived_Field__c;
            }
            else {
                queryFields = queryFields+', '+RTDetail.Next_Due_Date_Field__c+
                              ', '+RTDetail.Received_Date_Field__c+
                              ', '+RTDetail.Waived_Field__c;    
                }          
        }
        
        ID acctID;
        try{
            Contact c = [select Id, AccountId from Contact where ID = :[select ContactId from User where id = :UserInfo.getUserID()].ContactID];
            acctId = c.AccountId;
        }
        catch (exception e){}
        
        list<WIFIA_Loan__c> allLoans;
        
        if (isCommunityUser){
            if (acctId != null) allLoans = Database.query('Select id, name, '+queryFields+' from WIFIA_Loan__c where Borrower_Name__c = :acctId');
        }
        else{
            //return loans based on passed borrower name
            if (borrowerName != null) allLoans = Database.query('Select id, name, '+queryFields+' from WIFIA_Loan__c where Borrower_Name__r.Name = :borrowerName');
            else allLoans = Database.query('Select id, name, '+queryFields+' from WIFIA_Loan__c');
        }
        
        return allLoans;
    }
    
        @AuraEnabled
      // This action is initiated when component is opened
    public static List<RTList> getRTdetailsList(List<WIFIA_Loan__c> uLoans, boolean isCommunityUser) {
        List<RTList> RTdetails;
        list<RecordTypeDetails__c> retRTdeets = WIFIA_UtilityMethods.getRTDetails();
        
        if(RTdetails == null) {
            RTdetails = new List<RTList>();
            for(RecordTypeDetails__c retRTdeet : retRTdeets) {
            	if((isCommunityUser && !retRTdeet.WIFIA_Only__c) || (!isCommunityUser)){
                	RTdetails.add(new RTList(retRTdeet, uLoans));
            	}
            }
        }
        system.debug('RTdetails: '+RTdetails);
        RTdetails.sort();
        
        return RTdetails;
    }
    
    // Inner class to get details for getRTdetailsList
    public class RTList implements comparable{
        @AuraEnabled
        public RecordTypeDetails__c RTDetail;
        public list<WIFIA_Loan__c> uLoans;
        public string relDueLoanID;
        public string relDueLoanName;
        
        public RTList(RecordTypeDetails__c retRTdeet, List<WIFIA_Loan__c> userLoans) {
            this.RTDetail = retRTdeet;
            this.uLoans = userLoans;
        }
        
        public Integer compareTo(Object compareTo){
            RTList compareRTD = (RTList)compareTo;
            if(RTDetail.List_Order__c > compareRTD.RTDetail.List_Order__c) {return 1;}
            else if(RTDetail.List_Order__c < compareRTD.RTDetail.List_Order__c) {return -1;}
            else {return 0;}
        }

        // Properties for use in the Visualforce view 
        @AuraEnabled
        public String rtid {
            get { return RTDetail.Record_Type_ID__c; }
        }
        
        @AuraEnabled
        public Boolean timeBased {
            get { return RTDetail.Time_Date_Based__c; }
        }
        
        @AuraEnabled
        public String Name {
            get { 
            return RTDetail.Name; }
        }
        
        @AuraEnabled
        public String Description {
            get { return RTDetail.Description__c; }
        }
        
        @AuraEnabled
        public String lastSubmitted {
            get {
                string recDateFieldName = RTDetail.Received_Date_Field__c;
                date lastSubmittedDate;
                date lastSubmittedDateFinal;
                string lastSubmittedString;
                for(WIFIA_Loan__c uLoan : uLoans){
                     lastSubmittedDate = (date)uLoan.get(recDateFieldName);
                     if(lastSubmittedDate != null){
                         if(lastSubmittedDateFinal == null){
                            lastSubmittedDateFinal = lastSubmittedDate; 
                         }
                         else {
                             if(lastSubmittedDate > lastSubmittedDateFinal){
                                 lastSubmittedDateFinal = lastSubmittedDate;
                             }
                         }
                     }                   
                }
                lastSubmittedString = string.valueOf(lastSubmittedDateFinal);
                return lastSubmittedString; 
            }
        }
        
        @AuraEnabled
        public String NextDue {
            get {
                string nextDueFieldName = RTDetail.Next_Due_Date_Field__c;
                string waivedFieldName = RTDetail.Waived_Field__c;
                boolean waived;
                date nextDueDate;
                date nextDueDateFinal;
                string nextDueString;
                relDueLoanID = '';
                relDueLoanName = '';
                for(WIFIA_Loan__c uLoan : uLoans){
                     waived = (boolean)uLoan.get(waivedFieldName);
                     if(!waived){
                         nextDueDate = (date)uLoan.get(nextDueFieldName);
                         if(nextDueDate != null){
                             if(nextDueDateFinal == null){
                                nextDueDateFinal = nextDueDate; 
                                 nextDueString = string.valueOf(nextDueDateFinal);
                                 relDueLoanID = uLoan.Id;
                                 relDueLoanName = uLoan.Name;     
                             }
                             else {
                                 if(nextDueDate > nextDueDateFinal){
                                     nextDueDateFinal = nextDueDate;
                                     nextDueString = string.valueOf(nextDueDateFinal);
                                     relDueLoanID = uLoan.Id;
                                     relDueLoanName = uLoan.Name;  
                                 }
                             }
                         }
                     }                   
                }
                return nextDueString; 
            }
        }
        
        @AuraEnabled
        public String nextDueLoanID {
            get { 
                if(relDueLoanID != ''){return relDueLoanID;}
                else {return null;}
            }
        }
        
        @AuraEnabled
        public String nextDueLoanName {
            get { 
                if(relDueLoanName != ''){return relDueLoanName;}
                else {return null;}
            }
        }

    }
   
    @AuraEnabled
      //This action is initiated when Submit button is pressed
    public static string createNewDocRecord(string rtID, string rtName, string borrowerName){
        system.debug('creating new WIFIA Document record');
        WIFIA_Document_Submission__c newDoc = new WIFIA_Document_Submission__c();
        newDoc.recordtypeid = rtID;
        if(borrowerName != null && borrowerName != ''){
            newDoc.Borrower_Name__c = [select id from Account where Name = :borrowerName and RecordType.DeveloperName = 'WIFIA_Account'].ID;
        }
        Insert newDoc;
        
        return newDoc.Id;
    }
    
    @AuraEnabled
      //This action is initiated after document uploaded and user selected related loan record(s)
    public static string createLoanDocJuncs(list<string> loanIDs, string docID, boolean cbi){
        //string message = 'no action taken';
        
        list<WIFIA_Document_Submission_Loan_Junction__c> DSLJs = new list<WIFIA_Document_Submission_Loan_Junction__c>();
        
        for(string lID : loanIDs){
            WIFIA_Document_Submission_Loan_Junction__c DSLJ = new WIFIA_Document_Submission_Loan_Junction__c();
            DSLJ.WIFIA_Document_Submission__c = docID;
            DSLJ.WIFIA_Loan__c = lID;
            DSLJs.add(DSLJ);
        }
        
        Database.SaveResult[] srList = Database.insert(DSLJs, false);
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted doc/loan object. Record ID: ' + sr.getId());
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Doc/Loan Junction record fields that affected this error: ' + err.getFields());
                }
            }
        }

        //marks document as cbi if cbi = true
        system.debug('cbi: '+cbi);
        if(cbi){
            update new WIFIA_Document_Submission__c(id = docID, Document_is_CBI__c = true);
        }
        
        //if borrower name not on document submission, get from loan
        WIFIA_Document_Submission__c docSub = [select id, Borrower_Name__c from WIFIA_Document_Submission__c where id = :docID limit 1];
        if(docSub.Borrower_Name__c == null || docSub.Borrower_Name__c == ''){
            string relBorrowerID = [select id, Borrower_Name__c from WIFIA_Loan__c where id = :loanIDs[0] limit 1].Borrower_Name__c;
           // docSub.Borrower_Name__c = loanIDs[0];  
           // update docSub; 
            update new WIFIA_Document_Submission__c(id = docID, Borrower_Name__c = relBorrowerID);
        }
        
        return null;
    }
    
    @AuraEnabled
      //This action is initiated when user hits Cancel button on document upload
    public static string docCancel(string docID) {
        system.debug('canceling file upload '+docID);
        string confirm;
        
        try{
      //      WIFIA_Document_Submission__c doc = [select id from WIFIA_Document_Submission__c where id = :docID limit 1];
      //      delete doc;
            delete new WIFIA_Document_Submission__c(id = docID);
            confirm = 'deleted';
        }
        catch(exception e){
            system.debug('error: '+e);
            confirm = 'error: '+e;
        }
        
        return confirm;
         
    }
    
    @AuraEnabled
      // This action is initiated when component is opened
    public static list<WIFIA_Document_Submission_Loan_Junction__c> getLastSubmitted(list<WIFIA_Loan__c> userLoans) {
        system.debug('in getLastSubmitted');
        
        list<id> loanIDs = new list<id>();
        for(WIFIA_Loan__c loan : userLoans){
            loanIDs.add(loan.id);
        }
        
        
        list<WIFIA_Document_Submission_Loan_Junction__c> juncs = [select id, Document_Record_Type_ID__c, WIFIA_Loan__c, createddate 
                                                            from WIFIA_Document_Submission_Loan_Junction__c
                                                            where WIFIA_Loan__c in :loanIDs order by createddate DESC];

        return juncs;                                                            
    }

    @AuraEnabled
    public static List<Account> getBorrowers(String nameStart){
        return [select id, Name from Account where RecordType.DeveloperName = 'WIFIA_Account' and Name like :nameStart +'%'];
    }
}