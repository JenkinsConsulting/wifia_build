public class WIFIA_MetadataMethods {

    @AuraEnabled
    //Call for full list of all items
    public static List<LayoutItem> getMetaDataLayout(String layoutName){
        return getMetaDataLayout(layoutName,-1);
    }
    
    @AuraEnabled
    //Call for specific section of items
    public static List<LayoutItem> getMetadataLayout(String layoutName, integer sectionNum){
        List<String> componentNameList = new List<String>{layoutName};
        List<Metadata.Metadata> components = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, componentNameList);
        Metadata.Layout aLayout = (Metadata.Layout) components.get(0);
        
        return returnLayoutItems(alayout, sectionNum);
    }
    
    @AuraEnabled
    //generates list of LayoutItems
    public static List<LayoutItem> returnLayoutItems(Metadata.Layout alayout, integer sectionNum){
        List<LayoutItem> layoutItems = new List<LayoutItem>();
        
        if (sectionNum >= 0){
            MetaData.layoutSection ls = aLayout.layoutSections[sectionNum];
            layoutItems = returnInnerLayoutItems(layoutItems, ls, sectionNum);
        }
        else {
            sectionNum = 0;
            for (Metadata.LayoutSection ls : aLayout.layoutSections){
                layoutItems = returnInnerLayoutItems(layoutItems, ls, sectionNum);
                sectionNum++;
            }          
        }
        
        system.debug('layoutItems: '+layoutItems);
        system.debug('layoutItems length: '+layoutItems.size());
        return layoutItems;
    }
    
    @AuraEnabled
    //gets inner layour items from layout section
    public static List<LayoutItem> returnInnerLayoutItems(List<LayoutItem> layoutItems, MetaData.layoutsection ls, integer sectionNum){
        integer columnNum = 0;
        for (Metadata.LayoutColumn lc : ls.LayoutColumns){
            if (lc != null && lc.LayoutItems != null){
                integer fieldNum = 0;
                System.debug('Returning fields: ' + lc.LAyoutItems.size());
                for (Metadata.LayoutItem li : lc.LayoutItems){
                    if (li.field != null) layoutItems.add(new LayoutItem(li.field, sectionNum, columnNum, fieldNum, li.behavior.name() == 'Edit'));
                    else if (li.emptyspace) layoutItems.add(new LayoutItem('blank', sectionNum, columnNum, fieldNum, false));
                    fieldNum++;
                }
            }
            columnNum++;
        }
        
        system.debug('layoutItems(inner): '+layoutItems);
        system.debug('layoutItems(inner) length: '+layoutItems.size());
        return layoutItems;
    }
    
    public class LayoutItem{
        @AuraEnabled
        public string fieldName {get;set;}
        @AuraEnabled
        public integer sectionNum {get;set;}
        @AuraEnabled
        public integer columnNum {get;set;}
        @AuraEnabled
        public integer fieldNum {get;set;}
        @AuraEnabled
        public boolean editable {get;set;}
       
        public LayoutItem(string fieldName, integer sectionNum, integer columnnum, integer fieldNum, boolean editable){
            this.fieldName = fieldName;
            this.sectionNum = sectionNum;
            this.columnNum = columnNum;
            this.fieldNum = fieldNum;
            this.editable = editable;
        }
    }
    
    @AuraEnabled
    //generates list of layout section labels
    public static List<string> getLayoutSectionLabels(string layoutName){
        
        List<Metadata.Metadata> layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, new List<String> {layoutName});

        Metadata.Layout alayout = (Metadata.Layout)layouts.get(0);
        list<string> secLabels = new list<string>();
        for (Metadata.LayoutSection section : alayout.layoutSections) {
            secLabels.add(section.label);
        }
        
        return secLabels;
    }
    
}