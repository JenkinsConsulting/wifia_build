({
	doInit : function(component, event, helper) {
        console.log('in doInit');
        
        var action = component.get("c.MetadataLayoutSections");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
            	component.set("v.secLabels", response.getReturnValue());
            	
            	helper.getFields(component);              
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   
        
	},
	
	handleSelect : function(component, event, helper) {
		console.log('in handleSelect');
        
        var tab = event.getParam('id');
        console.log('selected tab: '+tab);
//        $A.get('e.force:refreshView').fire();
	}, 
	
	handleOnSuccess : function(component, event, helper) {
		helper.handleOnSuccess(component, event);
	},
	
	handleDiscard : function(component, event, helper) {
		helper.handleDiscard(component);
	},
	
	handleSave : function(component, event, helper) {
		helper.handleSave(component);
	},

	handleChange : function(component, event, helper) {
    	var editMode = component.get("v.editMode");
    	if(editMode){
	    	helper.handleChange(component);
    	}
    },
    
    handleViewAll : function(component, event, helper) {
    	helper.handleViewAll(component, event);
    },
    
    handleEdit : function(component, event, helper) {
    	helper.handleEdit(component, event);
    },
    
    reInit : function(component, event, helper) {
    	console.log('in reinit');
    }
	
})