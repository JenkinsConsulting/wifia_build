({
	getFields : function(component) {
		console.log('in getFields');
		
		var secLabels = component.get("v.secLabels");
		
		var action = component.get("c.MetadataLayoutSampleAll");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                component.set("v.retMdata", response.getReturnValue());
                this.buildTabs(component);	                
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   
	},
	
	buildTabs : function(component) {
		console.log('in buildTabs'); 
		var secLabels = component.get("v.secLabels");
		var resp = component.get("v.retMdata");
		var tabObj = [];
	
        for(var secs = 0; secs < secLabels.length; secs++){
        
	       	var colZero = [];
	        var colOne = [];
	        var fieldList = [];
	            var cols = '1';
	        // identifies column field belongs to and assigns to corresponding array
	        for(var i = 0; i < resp.length; i++){
	        	if(resp[i].sectionNum==secs){
		        	if(resp[i].columnNum == 0){
		                colZero.push(resp[i]);
		            }
		            else {
		                colOne.push(resp[i]);
		                cols = '2';
		            }
	        	}
	        }
	        
	        // creates final list in correct order using both arrays created in for loop above
	        for(var i = 0; i < colZero.length; i++){
	            fieldList.push(colZero[i]);
	            if(colOne[i] != null && colOne[i] != undefined){
	                fieldList.push(colOne[i]);
	            }
	        }    	              

	        if(fieldList.length != 0){
	        	var obj = new Object();
	            obj.label = secLabels[secs];
	            obj.id = secs;
	            obj.columns = cols;
	            obj.sectionFields = fieldList;
	            tabObj.push(obj);
	            
	        }
	        else {
	        	secLabels.splice(secs,1);
	        }

        } // end for loop of section labels
        
        component.set("v.tabs",tabObj);
        $A.get('e.force:refreshView').fire();
		
	},
	
	handleOnSuccess : function(component, event) {
		console.log('in helper.handleOnSuccess'); 
		
		var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(false);
        
        component.set("v.editMode", false);
        
		var record = event.getParam("response");
    	component.find("notifLib").showToast({
        "title": "Saved",
        "message": "Loan Record Saved",
        });
	},
	
	handleDiscard : function(component, event) {
        console.log('in helper.handleDiscard');
        
        component.set("v.editMode", false);
        
        var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(false);
    },
    
    handleSave : function(component, event) {
        console.log('in helper.handleSave');
        
        component.find("editForm").submit();
    	var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(false);
        
        component.set("v.editMode", false);
    },
    
    handleChange : function(component) {
    	console.log('in helper.handleChange');
    	
    	// turns on unsaved changes
        var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(true, { label: 'You have unsaved changes. Do you want to Continue?' });
    },
    
    handleViewAll : function(component, event) {
    	console.log('in helper.handleViewAll');
    	
    	var recID = component.get("v.recordId");
    	console.log('recID: '+recID);
    	var navService = component.find("navService");
  			
		var pageReference = {
								"type": "standard__component",
								"attributes": {
									"componentName": "c__WIFIA_Loan_New"
									},    
							    "state": {
							        "c__recordId": recID   
							    }
							}					
	
		event.preventDefault();
        navService.navigate(pageReference);
    },
    
    handleEdit : function(component, event) {
    	console.log('in helper.handleEdit');
    	
    	component.set("v.editMode", true);
    	
    	// turns on unsaved changes
 //       var unSavedChange = component.find('unsaved');
  //      unSavedChange.setUnsavedChanges(true, { label: 'You have unsaved changes. Do you want to Continue?' });
    	
	}
})