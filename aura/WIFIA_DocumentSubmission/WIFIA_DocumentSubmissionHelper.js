({
	retrieveLoans: function (component) {
	console.log('in helper.retrieveLoans');
	
	// this action retrieves a list of loans for this user as page loads
        // shows "no loans" error if no loans found
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.getLoans");
		action.setParams({
			"uID" : userId,
            "borrowerName": component.get("v.borrowerName"),
            "isCommunityUser" : component.get("v.isCommunityUser")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                var retLoans = response.getReturnValue();
                if(retLoans.length == 0){
                    component.set("v.pageStatus", "noLoans");
                }
                else {
                    component.set("v.userLoans", retLoans);
                }
                this.loadDocRTdetails(component);
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0 ;i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   
	},
	
	// this function is part of the init process when page loaded
	// load list of loans owned by user   <====== this will likely change to company
    loadDocRTdetails: function (component) {
        console.log('in helper.loadDocRTdetails');
       var commUser = component.get("v.isCommunityUser");
       var uloans = component.get("v.userLoans");
        // this action returns the list of WIFIA_Document_Submission__c	record types
        var action = component.get("c.getRTdetailsList");
		action.setParams({
			"uLoans" : uloans,
            "isCommunityUser" : commUser
        });
        action.setCallback(this, function(response){
            var name = response.getState();
            console.log("name: "+name);
            if (name === "SUCCESS") {
                var resp = response.getReturnValue();
                
	    		component.set("v.returnedDocumentRTs", response.getReturnValue());				               
                this.buildLoanOpts(component);               
            }
	    
        });
        $A.enqueueAction(action);       
    },
    
    // this function is part of the init process when page loaded
    // creates checkbox group of user's loans
    buildLoanOpts: function (component) {
        console.log('in helper.buildLoanOpts');
   		var opts = component.get("v.loanOptions");
        var opt;
        var retLoans = component.get("v.userLoans");
        console.log('retLoans: '+retLoans);
        for(var i = 0; i < retLoans.length;i++){
            console.log('retLoans iterate: '+retLoans[i].Id+', '+retLoans[i].Name);
            opt = {'label': retLoans[i].Name, 'value' : retLoans[i].Id};
            opts.push(opt);
        }
        console.log('opts: '+opts);
        
        component.set("v.loanOptions", opts); 
        this.organizeDocs(component);
    }, 
    
    // this function is part of the init process when page loaded
    // makes 2 lists of document types (scheduled & other)
    organizeDocs : function (component) {
        console.log('in helper.organizeDocs');
  		
        var retDocRTs = component.get("v.returnedDocumentRTs");
        var scheds = [];
    	var noScheds = [];
        
        // make lists of Doc Record Types for submission page
        // docs are separated if they are scheduled (aka time/date based)
        for(var i = 0; i<retDocRTs.length; i++){
            if(retDocRTs[i].timeBased == true){
                scheds.push(retDocRTs[i]);
            }
            else {noScheds.push(retDocRTs[i]);}
        } // end for loop
        component.set("v.documents", noScheds); 
        component.set("v.schedDocuments", scheds); 
    },
    
    // this function displays the list of WIFIA_Document_Submission__c record types
	// including a submit button and last submitted date for each
    showDocList : function(component) {
		console.log('in helper.showDocList');
       	
        component.set("v.relatedLoans", null);//clearing any previous info
        component.set("v.pageStatus", "viewDocumentTypes");
        console.log('setting page status to viewDocumentTypes');
	},
    
    // creates new WIFIA_Document_Submission__c record that will store file upload 
    submitNewDoc : function(component, event) {
		console.log('in helper.submitNewDoc');
       	
        var docRTID = event.currentTarget.id;
        console.log('doc RT ID: ' + docRTID);
        var docRTName = event.currentTarget.name;
        console.log('doc RT Name: ' + docRTName);
        var borrowerName = component.get("v.borrowerName");
            
        //create new WIFIA Document Submission Record
        var action = component.get("c.createNewDocRecord");
		action.setParams({
			"rtID" : docRTID,
            "rtName" : docRTName,
            "borrowerName" : borrowerName
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                var newDocID = response.getReturnValue();
                console.log('new doc record id: '+newDocID);
	    		component.set("v.docRecordID", newDocID);  
                
      //          console.log('setting the unsaved changes to true');
      //          var unsavedData = component.find('unsavedData');
      //    		unsavedData.setUnsavedChanges(true, {label: 'submitting document?'});
                
       /*         console.log('setting iframeUrl to formUpload VF page');
                var iFrameURL = '/apex/FileUpload?docID='+newDocID;
                component.set("v.iframeUrl", iFrameURL);
       */         
                component.set("v.submitDocType", docRTName);
                console.log('setting page status to newSubmission');
                component.set("v.pageStatus", "newSubmission");
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0 ;i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   
	},
    
    setCBI : function (component, event) {
        console.log('in helper.setCBI');
        var cbiChecker = event.getSource().get('v.checked');
        component.set("v.CBI", cbiChecker);
    },
    
    // sets list of loans that the submitted doc will be related to, then calls dml function
    submitLoanDoc : function (component, event) {
        console.log('in helper.submitLoanDoc');
        
        var loans = component.get("v.selectedLoans");
        var relLoans = component.get("v.relatedLoans");
        if(relLoans == null){relLoans = [];}
        var uLoans = component.get("v.userLoans");
        var relLoan;
            
        console.log('uLoans.length: '+uLoans.length);

        if(uLoans.length>1){
        for(var x = 0; x < loans.length;x++){
	        for(var i = 0; i < uLoans.length;i++){
    	        if(uLoans[i].Id == loans[x]){
                    relLoan = {'label': uLoans[i].Name, 'value': uLoans[i].Id};
                    relLoans.push(relLoan);
                }
            }
        }
        }
        else {
            relLoan = {'label': uLoans[0].Name, 'value': uLoans[0].Id};
                    relLoans.push(relLoan);
        }

        component.set("v.relatedLoans", relLoans);
        this.LoanDocDML(component);
	},
    
    // after document uploaded, loan/document junction record(s) created
    LoanDocDML : function (component, event) {
        console.log('in helper.LoanDocDML');
        
        var loanIDs = [];
        var relLoans = component.get("v.relatedLoans");
        var docID = component.get("v.docRecordID");
        var cbi = component.get("v.CBI");

        if(relLoans != null){
        	for(var x = 0; x < relLoans.length;x++){
            	loanIDs.push(relLoans[x].value);
        	}
        }
    
        if(loanIDs.length != 0){
        	var action = component.get("c.createLoanDocJuncs");
			action.setParams({
	            "loanIDs" : loanIDs,
	            "docID" : docID,
	            "cbi" : cbi
	        });
	        action.setCallback(this, function(response){
	            var state = response.getState();
	            console.log("state: "+state);
	            var message = response.getReturnValue();
	            console.log("message: "+message);
	            if (state === "SUCCESS") { 
	        //        var unsavedData = component.find('unsavedData');
	        //		unsavedData.setUnsavedChanges(false, {label: 'document submitted'});
	        //        console.log('setting unsaved changes to false');
	                component.set("v.pageStatus", "success");
	            }
	            else if(state === 'ERROR'){
	            	var errors = response.getError();
	            	for(var i = 0 ;i < errors.length;i++){
	                	console.log(errors[i].message);
	                	console.log(errors[i]);
	            	}
	            }
	        });
	        $A.enqueueAction(action);
        } // end if loanIDs != 0
    },
    
    // this method fires when user hits "cancel" button during document upload    
    cancel : function (component) {
    	console.log('in helper.cancel');
        
        var docID = component.get("v.docRecordID");
        console.log('docID: '+docID);
        
        // delete WIFIA_Document_Submission__c record
        var action = component.get("c.docCancel");
		action.setParams({
            "docID" : docID
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                component.set("v.pageStatus", "init");
                this.runDoInit(component);
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0 ;i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);        
    },
	
    handleUploadFinished : function (component) {
    	console.log('in helper.handleUploadFinished');
        
        var retLoans = component.get("v.userLoans");
        if(retLoans.length == 1){this.submitLoanDoc(component);}
		else {component.set("v.pageStatus", "selectLoans");}
	},
    
    runDoInit : function (component) {
    	console.log('in helper.handleUploadFinished');
    	
        var action = component.get("c.doInit")
        $A.enqueueAction(action);  
    },
    
    // this method fires when the user checks/unchecks loan(s) to relate the uploaded file with
    // sets values (loan IDs) for WIFIA_Document_Submission_Loan_Junction__c junction object record creation
    handleChange : function (component, event) {
        console.log('in helper.handleChange');
        component.set("v.selectedLoans", event.getParam('value'));
    },
    
    // this method fires when user hits "Submit Another Document" button
    resetAll : function (component){
        console.log('in helper.resetAll');
        
        window.open("/lightning/n/Submit_WIFIA_Document","_self");
    },
    
    beforeUnloadHandler : function (component, event) {
 		console.log('before unload handler has been called.');
        this.cancel(component);
	}
        
})