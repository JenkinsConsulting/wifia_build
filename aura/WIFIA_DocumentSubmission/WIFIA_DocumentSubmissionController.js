({
	/**
     * @author Innovate
     * @purpose Called during initialization of the component
     *
     ********* History*********
     *
     */
    doInit : function(component, event, helper) {
        console.log('in doInit action');
		
        // adds listener in case user leaves page mid-upload, runs cancel function
        window.addEventListener('beforeunload', helper.beforeUnloadHandler.bind(helper));
        
        // this action returns the user's profile to determine if Community User
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.getUserProfile");
		action.setParams({
			"uID" : userId
        });
        action.setCallback(this, function(response){
            var name = response.getState();
            console.log("name: "+name);
            if (name === "SUCCESS") {
                var resp = response.getReturnValue();
                if(resp.indexOf("Community")<0 && resp.indexOf("community")<0){
		    		console.log('is not community user');
		    		component.set("v.isCommunityUser", false);		
		    	}
		    	else {
		    		console.log('is community user');
		    		component.set("v.isCommunityUser", true);
		    	}	
	    				               
                helper.retrieveLoans(component);               
            }
	    
        });
        $A.enqueueAction(action);

	},
    
	// this method fires when user clicks "Submit New Document" or "Submit Another Document" buttons    
    showDocList : function(component, event, helper) {
        console.log('in showDocList');
        helper.showDocList(component);
    },
    
    // this method fires when user clicks "Submit" button to upload a file from the
     // list of WIFIA_Document_Submission__c record types 
    submitNewDoc : function(component, event, helper) {
        console.log('in submitNewDoc');
        helper.submitNewDoc(component, event);
    },
    
    handleChange : function (component, event, helper) {
        helper.handleChange(component, event);
    }, 
    
    submitLoanDoc : function (component, event, helper) {
        console.log('in submitLoanDoc');
        helper.submitLoanDoc(component);
    },
    
    setCBI : function (component, event, helper) {
        console.log('in setCBI');
        helper.setCBI(component, event);
    },
    
    handleUploadFinished : function (component, event, helper) {
        console.log('in handleUploadFinished');      
        helper.handleUploadFinished(component);
    },
    
    cancel : function (component, event, helper) {
        console.log('in cancel');
        
        helper.cancel(component);
    },
    
    submitAnother : function (component, event, helper) {
        console.log('in submitAnother');
        helper.resetAll(component);
    },
    getVals: function(component, event, helper) {
        const searchString = component.get('v.borrowerName');
        var action = component.get("c.getBorrowers");
		action.setParams({
			"nameStart" : searchString
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                component.set("v.predictions", response.getReturnValue());
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0 ;i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);  
    },
    getDetails: function(component, event, helper) {
        var selectedItem = event.target.id;
        console.log(selectedItem);
        component.set('v.borrowerName', selectedItem);
        component.set('v.predictions', "");
    }
    
})