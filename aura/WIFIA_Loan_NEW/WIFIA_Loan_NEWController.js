({
	doInitAll : function(component, event, helper) {
        console.log('in doInitAll');
       
        component.set("v.IsSpinner",true);
  
  		var pageReference = component.get("v.pageReference"); 
  		console.log('pageReference: '+pageReference);
  		var recID = pageReference.state.c__recordId;
  		console.log('recID: '+recID);
		component.set("v.recordId", pageReference.state.c__recordId);
		
        var action = component.get("c.MetadataLayoutSections");
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
            	var secLabels = response.getReturnValue();
            	helper.getFields(component, secLabels);              
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   
        
	},
	
	handleSelect : function(component, event, helper) {
		console.log('in handleSelect');
        
        var tab = event.getParam('id');
        console.log('tab: '+tab);
	}, 
	
	handleOnSuccess : function(component, event, helper) {
		helper.handleOnSuccess(component, event);
	},
	
	handleDiscard : function(component, event, helper) {
		helper.handleDiscard(component);
	},
	
	handleSave : function(component, event, helper) {
		helper.handleSave(component);
	},

	handleChange : function(component, event, helper) {
		var editMode = component.get("v.editMode");
    	if(editMode){
	    	helper.handleChange(component);
    	}
    },
    
    handleSaveButton : function(component, event, helper) {
    	helper.handleSaveButton(component);
    //	helper.handleSave(component);
    },
    
    showSpinner : function(component, event, helper) {
    	helper.showSpinner(component);
    },
    
    hideSpinner : function(component, event, helper) {
    	helper.hideSpinner(component);
    },
    
    loadFinished : function(component, event, helper) {
    	helper.loadFinished(component);
    },
    
    handleViewDetails : function(component, event, helper) {
    	helper.handleViewDetails(component, event);
    },
    
    handleViewAmortTool : function(component, event, helper) {
    	helper.handleViewAmortTool(component, event);
    },
    
    handleEdit : function(component, event, helper) {
    	helper.handleEdit(component, event);
    }
	
})