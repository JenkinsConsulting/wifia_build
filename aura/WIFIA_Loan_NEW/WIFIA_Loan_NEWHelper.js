({
	getFields : function(component, secLabels) {
		console.log('in getFields');
		
			var action = component.get("c.MetadataLayoutSampleAll");
	        action.setCallback(this, function(response){
	            var state = response.getState();
	            console.log("state: "+state);
	            if (state === "SUCCESS") {
	                var resp = response.getReturnValue();
	                this.buildTabs(component, secLabels, resp);	                
	            }
	            else if(state === 'ERROR'){
	            	var errors = response.getError();
	            	for(var i = 0; i < errors.length;i++){
	                	console.log(errors[i].message);
	            	}
	            }
	        });
	        $A.enqueueAction(action);   
	},
	
	buildTabs : function(component, secLabels, resp) {
		console.log('in helper.buildTabs'); 
		
		var tabObj = [];
		
        for(var secs = 0; secs < secLabels.length; secs++){
        
	       	var colZero = [];
	        var colOne = [];
	        var fieldList = [];
	            var cols = '1';
	        // identifies column field belongs to and assigns to corresponding array
	        for(var i = 0; i < resp.length; i++){
	        	if(resp[i].sectionNum==secs){
		        	if(resp[i].columnNum == 0){
		                colZero.push(resp[i]);
		            }
		            else {
		                colOne.push(resp[i]);
		                cols = '2';
		            }
	        	}
	        }
	        
	        // creates final list in correct order using both arrays created in for loop above
	        for(var i = 0; i < colZero.length; i++){
	            fieldList.push(colZero[i]);
	            if(colOne[i] != null && colOne[i] != undefined){
	                fieldList.push(colOne[i]);
	            }
	        }    	              

	        if(fieldList.length != 0){
	        	var obj = new Object();	            
	            obj.label = secLabels[secs];
	            obj.id = secs;
	            obj.columns = cols;
	            obj.sectionFieldsAll = fieldList;
	            tabObj.push(obj);
	        }
	        else {
	        	secLabels.splice(secs,1);
	        }

        } // end for loop of section labels
        
        component.set("v.lastTabID", obj.id);
        component.set("v.tabsAll",tabObj);
       
        this.loadFinished(component);

	},
	
	handleOnSuccess : function(component, event) {
		console.log('in handleOnSuccess'); 
		
		var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(false);
        
        component.set("v.editMode", false);
        
	//	var record = event.getParam("response");
    	component.find("notifLib").showToast({
        "title": "Saved",
        "message": "Loan Record Saved",
        });
	},
	
	handleDiscard : function(component, event) {
        console.log('in helper.handleDiscard');
        var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(false);
        
        component.set("v.editMode", false);
    },
    
    handleSave : function(component, event) {
        console.log('in helper.handleSave');
        
        component.set("v.editMode", false);
        
        component.find("editFormTab").submit();
    	var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(false);
    },
    
    handleChange : function(component) {
    	console.log('in helper.handleChange');
    	
    	// turns on unsaved changes
        var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(true, { label: 'You have unsaved changes. Do you want to Continue?' });
    },
    
    handleSaveButton : function(component, event) {
        console.log('in helper.handleSaveButton');
        
        
        component.find("editFormAll").forEach( form =>{
        	try{
            form.submit();
        	}
        	catch(e){
        		console.log('e: '+e);
        		console.log('form.id: '+form.id);
        	}
        })
        
    	var unSavedChange = component.find('unsaved');
        unSavedChange.setUnsavedChanges(false);
        
        component.set("v.editMode", false);
        this.handleOnSuccess(component, event);
    },
    
    showSpinner : function(component) {
    	console.log('in helper.showSpinner');
    	component.set("v.IsSpinner",true);
    },
    
    hideSpinner : function(component) {
    	console.log('in helper.hideSpinner');
    	component.set("v.IsSpinner",false);
    },
    
    loadFinished : function(component) {
    	console.log('in helper.loadFinished');
    	window.setTimeout(
    		    $A.getCallback(function() {
    		    	console.log('turning off spinner');
    		    	component.set("v.IsSpinner",false);
    		    }), 3000
    		);
    },
    
    handleViewDetails : function(component, event) {
    	console.log('in helper.handleViewDetails');
    	
 		var recID = component.get("v.recordId");
    	console.log('recID: '+recID);
		
		var recID = component.get("v.recordId");
		var action = component.get("c.saveLoan");
        action.setParams({
            "LoanID" : recID,
            "saveAction" : "clear amort"
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                console.log('save status: '+response.getReturnValue());
                var loanURL = "/"+recID;
		 		window.location.replace(loanURL);
            }
            else if(state === 'ERROR'){
                var errors = response.getError();
                for(var i = 0; i < errors.length;i++){
                    console.log(errors[i].message);
                }
            }
        });
        $A.enqueueAction(action); 
  		
    },
    
    handleViewAmortTool : function(component, event) {
    	console.log('in helper.handleViewAmortTool');
    	
 		var recID = component.get("v.recordId");
    	console.log('recID: '+recID);
    	
    	var action = component.get("c.saveLoan");
        action.setParams({
            "LoanID" : recID,
            "saveAction" : "set amort"
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                console.log('save status: '+response.getReturnValue());
                var loanURL = "/"+recID;
				window.location.replace(loanURL);
            }
            else if(state === 'ERROR'){
                var errors = response.getError();
                for(var i = 0; i < errors.length;i++){
                    console.log(errors[i].message);
                }
            }
        });
        $A.enqueueAction(action); 
		
    },
    
    handleEdit : function(component, event) {
    	console.log('in helper.handleEdit');
    	
    	component.set("v.editMode", true);
    	
    	// turns on unsaved changes
 //       var unSavedChange = component.find('unsaved');
  //      unSavedChange.setUnsavedChanges(true, { label: 'You have unsaved changes. Do you want to Continue?' });
    	
	}
    
})