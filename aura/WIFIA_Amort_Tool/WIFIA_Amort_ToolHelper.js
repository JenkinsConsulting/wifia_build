({
	loadLoanInfo : function(component) {
		console.log('in loadLoanInfo');
		var loanID = component.get("v.recordId");
		
		//get existing loan data
		var action = component.get("c.getLoanInfo");
		action.setParams({
			"loanID" : loanID
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                var loanInfo = response.getReturnValue();

                component.set("v.loanInfo", loanInfo);
                if(loanInfo.Amort_Redirect__c){
                	var loanID = loanInfo.Id;
                	this.clearRedirect(component, loanID);
                }
                else{
                	this.createDraws(component);
            	}
            	
            	component.set("v.repaymentSchedMonthLabel", loanInfo.Repayment_Schedule_Months__c);
            	component.set("v.repaymentSchedDayLabel", loanInfo.Repayment_Schedule_Day__c);
            	
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action); 
	
	},
	
	clearRedirect : function(component, loanID) {
		console.log('in helper.clearRedirect');
		
		var action = component.get("c.saveLoan");
        action.setParams({
            "LoanID" : loanID,
            "saveAction" : "clear amort"
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                console.log('save status: '+response.getReturnValue());
                this.createDraws(component);
            }
            else if(state === 'ERROR'){
                var errors = response.getError();
                for(var i = 0; i < errors.length;i++){
                    console.log(errors[i].message);
                    this.createDraws(component);
                }
            }
        });
        $A.enqueueAction(action); 
		
	},
	
	createDraws : function(component) {
		console.log('in helper.createDraws');
		
		var disburs = component.get("v.retDisburs");
		var drawObj = [];
		
		var loanInfo = component.get("v.loanInfo");
		var repayDay = loanInfo.Repayment_Schedule_Day__c;
		var loanClose = new Date(loanInfo.Loan_Close_Date__c);
		var todayDate = new Date();
	
		for(var i = 0; i < disburs.length; i++){
			var obj = new Object();
			obj.msg = "";
            obj.indexVar = disburs[i].Id;
            //obj.Date_Received = disburs[i].Executed_Draw_Date__c;
            obj.drawDate = disburs[i].Executed_Draw_Date__c;
            //obj.Payment_Amount = disburs[i].Executed_Draw_Amount__c;
            obj.anticipatedAmount = 0;
            obj.executedAmount = disburs[i].Executed_Draw_Amount__c;
            
            
			var getDdate = disburs[i].Executed_Draw_Date__c;
			if(getDdate){
				var ddate = new Date(getDdate.substring(5,7)+"-"+getDdate.substring(8,10)+"-"+getDdate.substring(0,4));
				var dday = ddate.getDate();
				console.log('dday1: '+dday);
			}
			obj.msg = "";
			
            if(repayDay=="1st of Month" && dday != 1 && dday != 15){
            	obj.msg = "ERROR: draw must be on the 1st or 15th of the month";
            }
            else if(repayDay=="15th of Month" && dday != 15){
            	obj.msg = "ERROR: draw must be on the 15th of the month";
            }
            
            if(ddate < loanClose){
            	if(obj.msg==""){
            	obj.msg = "ERROR: Draw must be after Loan Closing Date";
            	}
            	else{obj.msg = obj.msg + " / " + "ERROR: Draw must be after Loan Closing Date";}
            }
            
            if(obj.anticipatedAmount > 0 && obj.executedAmount > 0){
            	if(obj.msg==""){
            	obj.msg = "ERROR: Date cannot have anticipated and executed amounts";
            	}
            	else{obj.msg = obj.msg + " / " + "ERROR: Date cannot have anticipated and executed amounts";}
            }
            
            if(obj.anticipatedAmount > 0 && ddate <= todayDate){
            	if(obj.msg==""){
	            	obj.msg = "ERROR: Anticipated Draw must be in the future";
            	}
            	else{obj.msg = obj.msg + " / " + "ERROR: Anticipated Draw must be in the future";}
            }
            
            if(obj.executedAmount > 0 && ddate > todayDate){
     			if(obj.msg==""){
     		       	obj.msg = "ERROR: Executed Draw can't  be in the future";
            	}
            	else{obj.msg = obj.msg + " / " + "ERROR: Executed Draw can't  be in the future";}
            }
            
            drawObj.push(obj);
		}
		
		var drawCt = (drawObj.length -1);
		
		component.set("v.draws",drawObj);
		component.set("v.drawCount", drawCt);
		this.getRepaymentTypes(component);
	},
		
	getRepaymentTypes : function(component) {
		console.log('in helper.getRepaymentTypes');
		
		//get Repayment Types for picklist
		var repayTypesList = ["No Payment", "Interest Only Total", "Level Payment (semiannual)", "Fixed Principal ($)", "Fixed Principal (%)", "Partial Payment P&I ($)", "Partial Payment P&I (%)", "Level Payment (annual p, semiannual i)", "Level Payment (one payment per year)"];
        component.set("v.repaymentTypes", repayTypesList);
        
        this.buildrepaymentSchedMonthOptions(component);   
	},
	
	buildrepaymentSchedMonthOptions : function(component) {
		console.log('in helper.buildrepaymentSchedMonthOptions');
		
		//create Repayment Schedule Months for picklist
	//	var repaySchedMList = ["January-July", "February-August", "March-September", "April-October", "May-November", "June-December"];
	//	component.set("v.repaymentSchedMonthOptions", repaySchedMList);
		
		//if Repayment Schedule Month set on loan field, update component values
		var loanInfo = component.get("v.loanInfo");
		if(loanInfo.Repayment_Schedule_Months__c != null) {
			var selected = loanInfo.Repayment_Schedule_Months__c;
	//		component.set("v.repaymentSchedMonthLabel", selected);
	//		component.find("repaySchedMonthSelect").set("v.value", selected);
			
	
			var peMonth1;
			var peMonth2;
			var rmMonth1;
			var rmMonth2;
			
			var fmd = loanInfo.Final_Maturity_Date__c;
			if(fmd){
				var fmdMonth = fmd.substring(5, 7);
				fmdMonth = parseInt(fmdMonth);
				var fmdDay = fmd.substring(8, 10);
				fmdDay = parseInt(fmdDay);
			}
			
			switch (selected) {
				case "January-July":
						peMonth1 = 6;
						peMonth2 = 12;
						rmMonth1 = 1;
						rmMonth2 = 7;
						if(fmdMonth != rmMonth1 && fmdMonth != rmMonth2){
							component.set("v.matDateMsg", "Error: month must be consistent with repayment schedule");
						}
						break;
						
				case "February-August":
						peMonth1 = 1;
						peMonth2 = 7;
						rmMonth1 = 2;
						rmMonth2 = 8;
						if(fmdMonth != rmMonth1 && fmdMonth != rmMonth2){
							component.set("v.matDateMsg", "Error: month must be consistent with repayment schedule");
						}
						break;
				
				case "March-September":
						peMonth1 = 2;
						peMonth2 = 8;
						rmMonth1 = 3;
						rmMonth2 = 9;
						if(fmdMonth != rmMonth1 && fmdMonth != rmMonth2){
							component.set("v.matDateMsg", "Error: month must be consistent with repayment schedule");
						}
						break;
						
				case "April-October":
						peMonth1 = 3;
						peMonth2 = 9;
						rmMonth1 = 4;
						rmMonth2 = 10;
						if(fmdMonth != rmMonth1 && fmdMonth != rmMonth2){
							component.set("v.matDateMsg", "Error: month must be consistent with repayment schedule");
						}
						break;
						
				case "May-November":
						peMonth1 = 4;
						peMonth2 = 10;
						rmMonth1 = 5;
						rmMonth2 = 11;
						if(fmdMonth != rmMonth1 && fmdMonth != rmMonth2){
							component.set("v.matDateMsg", "Error: month must be consistent with repayment schedule");
						}
						break;
						
				case "June-December":
						peMonth1 = 5;
						peMonth2 = 11;
						rmMonth1 = 6;
						rmMonth2 = 12;
						if(fmdMonth != rmMonth1 && fmdMonth != rmMonth2){
							component.set("v.matDateMsg", "Error: month must be consistent with repayment schedule");
						}
						break;				
			}
			component.set("v.repaymentSchedPEmonth1", peMonth1); // "Repayment Period End Month 1" on amort tool spreadsheet (Reference Tables tab)
			component.set("v.repaymentSchedPEmonth2", peMonth2); // "Repayment Period End Month 2" on amort tool spreadsheet (Reference Tables tab)
			component.set("v.repaymentSchedRMmonth1", rmMonth1); // "Repayment Month 1" on amort tool spreadsheet (Reference Tables tab)
			component.set("v.repaymentSchedRMmonth2", rmMonth2); // "Repayment Month 2" on amort tool spreadsheet (Reference Tables tab)
		}
		
		this.buildFilePurposeOptions(component);
	},
/*	
	buildrepaymentSchedDayOptions : function(component) {
		console.log('in helper.buildrepaymentSchedDayOptions');
		
		//create Repayment Schedule Days for picklist
		var repaySchedDList = ["1st of Month", "15th of Month"];
		component.set("v.repaymentSchedDayOptions", repaySchedDList);
		
		//if Repayment Schedule Day set on loan field, update component values
		var loanInfo = component.get("v.loanInfo");
		if(loanInfo.Repayment_Schedule_Day__c != null) {
			var selected = loanInfo.Repayment_Schedule_Day__c;			
			component.set("v.repaymentSchedDayLabel", selected);
			component.find("repaySchedDaySelect").set("v.value", selected);
		}
		
		this.buildFilePurposeOptions(component);
	},
	*/
	
	buildFilePurposeOptions : function(component) {
		console.log('in helper.buildFilePurposeOptions');
		
		//create File Purpose Options for picklist
		var filePurposeList = ["Underwriting or Re-estimates", "Loan Management (post close)"];
		
		component.set("v.filePurposeOptions", filePurposeList);
		
		this.createLoanSculptingTable(component);

	},
	
	deleteDraw : function(component, event) {
		console.log('in helper.deleteDraw');

		var drawID = event.getSource().get("v.name");
		
		var drawObj = [];
		drawObj = component.get("v.draws");
		drawObj.splice(drawID,1);

		component.set("v.draws",drawObj);
		
		var totalDraws = 0;       
        var draws = component.get("v.draws");
        for(var i = 0; i < draws.length; i++){
            totalDraws = totalDraws + parseFloat(draws[i].anticipatedAmount);
            totalDraws = totalDraws + parseFloat(draws[i].executedAmount);
   		}
		
		component.set("v.totalDraws", totalDraws);
		
	},
	
	addDraw : function(component, event) {
		console.log('in helper.addDraw');
		
		var today = new Date();
		var randID = String(today.getDate())+String(today.getMinutes())+String(today.getSeconds());
		
		
		var drawObj = [];
		drawObj = component.get("v.draws");
		
		var obj = new Object();
		obj.indexVar = randID;
		obj.drawDate;
        obj.anticipatedAmount = 0;
        obj.executedAmount = 0;
        drawObj.push(obj);
        
        component.set("v.draws",drawObj);
		$A.get('e.force:refreshView').fire();
	},
	
	// no longer needed //
	saveDraw : function(component, event) {
		console.log('in helper.saveDraw');
		
		var loanID = component.get("v.recordId");
		console.log('loanID: '+loanID);
		
		var drawID = event.getSource().get("v.name");		
		console.log('drawID: '+drawID);
		
		var newDraw = component.get("v.draws");
		var drawDate = newDraw[drawID].Date_Received;
		console.log('drawDate: '+drawDate);
		var drawAmt = newDraw[drawID].Payment_Amount;
		console.log('drawAmt: '+drawAmt);
		
		//create new WIFIA Payment record
		var action = component.get("c.createNewDisbursement");
        action.setParams({
            "loanID" : loanID,
            "drawDate" : drawDate,
            "drawAmt" : drawAmt
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
            	var drawCT = component.get("v.drawCount");
            	drawCT = drawCT+1;
                component.set("v.drawCount", drawCT);
                component.set("v.newDrawID", response.getReturnValue());
                console.log('new draw id: '+response.getReturnValue());
                component.set("v.isDrawModalOpen", true);
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   

	},
	
	// no longer needed //
	updateDraw : function(component, event) {
		console.log('in helper.updateDraw'); 
	
		component.find("recordEditForm").submit();
	},
	
	// no longer needed //
	cancelSaveDraw : function(component, event) {
		console.log('in helper.cancelSaveDraw'); 
		
		var drawID = component.get("v.newDrawID");
		
		//delete new payment created for the saveDraw process
		var action = component.get("c.deleteNewDisbursement");
        action.setParams({
            "drawID" : drawID
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
            	var drawCT = component.get("v.drawCount");
           // 	drawCT = drawCT-1;
                component.set("v.drawCount", drawCT);
                component.set("v.newDrawID", "");
                component.set("v.isDrawModalOpen", false);
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   
	
	},
	
	handleOnDrawSaveSuccess : function(component, event) {
		console.log('in helper.handleOnDrawSaveSuccess'); 
        
        component.set("v.isDrawModalOpen", false);
        
		//var record = event.getParam("response");
    	component.find("notifLib").showToast({
        "title": "Saved",
        "message": "New WIFIA Payment Saved",
        });
	},
	
	saveDrawDate : function(component, event) {
		console.log('in helper.saveDrawDate');
		
		var drawID = event.getSource().get("v.name");
		var drawValue = event.getSource().get("v.value");
		
		console.log('drawID: '+drawID);
		console.log('drawValue: '+drawValue);
		
		var drawObj = [];
		drawObj = component.get("v.draws");
		
		var obj = new Object();
		obj.indexVar = drawObj[drawID].indexVar;
		obj.drawDate = drawValue;
        obj.anticipatedAmount = drawObj[drawID].anticipatedAmount;
        obj.executedAmount = drawObj[drawID].executedAmount;
        drawObj.splice(drawID,1,obj);
        
        //validations
        var loanInfo = component.get("v.loanInfo");
		var repayDay = loanInfo.Repayment_Schedule_Day__c.trim();
		var loanClose = new Date(loanInfo.Loan_Close_Date__c);
		var todayDate = new Date();
		
		var udate = new Date(obj.drawDate);
		var ddate = new Date(udate.getTime() + udate.getTimezoneOffset() * 60000);
		var dday = ddate.getDate();
		obj.msg = "";
		
		if(repayDay==="1st of Month" && dday != 1 && dday != 15){
			obj.msg = "ERROR: draw must be on the 1st or 15th of the month";
		}
		else if(repayDay==="15th of Month" && dday != 15){
			obj.msg = "ERROR: draw must be on the 15th of the month";
		}
		            
		if(ddate < loanClose){
			if(obj.msg==""){
				obj.msg = "ERROR: Draw must be after Loan Closing Date";
		        }
		        else{obj.msg = obj.msg + " / " + "ERROR: Draw must be after Loan Closing Date";}
		}
		            
		if(obj.anticipatedAmount > 0 && obj.executedAmount > 0){
			if(obj.msg==""){
				obj.msg = "ERROR: Date cannot have anticipated and executed amounts";
			}
			else{obj.msg = obj.msg + " / " + "ERROR: Date cannot have anticipated and executed amounts";}
		}
		            
		if(obj.anticipatedAmount > 0 && ddate <= todayDate){
			if(obj.msg==""){
				obj.msg = "ERROR: Anticipated Draw must be in the future";
			}
		        else{obj.msg = obj.msg + " / " + "ERROR: Anticipated Draw must be in the future";}
		}
		            
		if(obj.executedAmount > 0 && ddate > todayDate){
			if(obj.msg==""){
		     		obj.msg = "ERROR: Executed Draw can't  be in the future";
		        }
		        else{obj.msg = obj.msg + " / " + "ERROR: Executed Draw can't  be in the future";}
		}
        
        component.set("v.draws",drawObj);
		$A.get('e.force:refreshView').fire();
	},
	
	saveDrawAnticipatedAmount : function(component, event) {
		console.log('in helper.saveDrawAnticipatedAmount');
		
		var drawID = event.getSource().get("v.name");
		var drawValue = event.getSource().get("v.value");
		
		console.log('drawID: '+drawID);
		console.log('drawValue: '+drawValue);
		
		var drawObj = [];
		drawObj = component.get("v.draws");
		
		var obj = new Object();
		obj.indexVar = drawObj[drawID].indexVar;
		obj.drawDate = drawObj[drawID].drawDate;
		console.log('drawValue.length: '+drawValue.length);
		
		if(drawValue.length < 1){
			console.log('setting to zero');
			obj.anticipatedAmount = "0";
		}
		else{
			console.log('in else!');
			obj.anticipatedAmount = drawValue;
		}
		
		if(!drawObj[drawID].executedAmount){
			obj.executedAmount = "0";
		}
		else{
			obj.executedAmount = drawObj[drawID].executedAmount;
		}
        //obj.executedAmount = drawObj[drawID].executedAmount;

        drawObj.splice(drawID,1,obj);
        
        //validations
        var loanInfo = component.get("v.loanInfo");
		var repayDay = loanInfo.Repayment_Schedule_Day__c;
		var loanClose = new Date(loanInfo.Loan_Close_Date__c);
		var todayDate = new Date();
		
		
		var getDdate = obj.drawDate;
		if(getDdate){
			var ddate = new Date(getDdate.substring(5,7)+"-"+getDdate.substring(8,10)+"-"+getDdate.substring(0,4));
			var dday = ddate.getDate();
		}
		obj.msg = "";
		
		if(repayDay=="1st of Month" && dday != 1 && dday != 15){
			obj.msg = "ERROR: draw must be on the 1st or 15th of the month";
		}
		else if(repayDay=="15th of Month" && dday != 15){
			obj.msg = "ERROR: draw must be on the 15th of the month";
		}
		            
		if(ddate < loanClose){
			if(obj.msg==""){
				obj.msg = "ERROR: Draw must be after Loan Closing Date";
		        }
		        else{obj.msg = obj.msg + " / " + "ERROR: Draw must be after Loan Closing Date";}
		}
		            
		if(obj.anticipatedAmount > 0 && obj.executedAmount > 0){
			if(obj.msg==""){
				obj.msg = "ERROR: Date cannot have anticipated and executed amounts";
			}
			else{obj.msg = obj.msg + " / " + "ERROR: Date cannot have anticipated and executed amounts";}
		}
		            
		if(obj.anticipatedAmount > 0 && ddate <= todayDate){
			if(obj.msg==""){
				obj.msg = "ERROR: Anticipated Draw must be in the future";
			}
		        else{obj.msg = obj.msg + " / " + "ERROR: Anticipated Draw must be in the future";}
		}
		            
		if(obj.executedAmount > 0 && ddate > todayDate){
			if(obj.msg==""){
		     		obj.msg = "ERROR: Executed Draw can't  be in the future";
		        }
		        else{obj.msg = obj.msg + " / " + "ERROR: Executed Draw can't  be in the future";}
		}
        
        component.set("v.draws",drawObj);
        
        var totalDraws = 0;       
        var draws = component.get("v.draws");
        for(var i = 0; i < draws.length; i++){
				            totalDraws = totalDraws + parseFloat(draws[i].anticipatedAmount);
				            totalDraws = totalDraws + parseFloat(draws[i].executedAmount);
            		}
      component.set("v.totalDraws", totalDraws);
        
		$A.get('e.force:refreshView').fire();
	},
	
	saveDrawExecutedAmount : function(component, event) {
		console.log('in helper.saveDrawExecutedAmount');
		
		var drawID = event.getSource().get("v.name");
		var drawValue = event.getSource().get("v.value");
		
		console.log('drawID: '+drawID);
		console.log('drawValue: '+drawValue);
		
		var drawObj = [];
		drawObj = component.get("v.draws");
		
		var obj = new Object();
		obj.indexVar = drawObj[drawID].indexVar;
		obj.drawDate = drawObj[drawID].drawDate;
		
		if(!drawObj[drawID].anticipatedAmount){
			obj.anticpatedAmount = "0";
		}
		else{
			obj.anticipatedAmount = drawObj[drawID].anticipatedAmount;
		}
        //obj.anticipatedAmount = drawObj[drawID].anticipatedAmount;
        
        if(drawValue.length < 1){
			obj.executedAmount = "0";
		}
		else{
			obj.executedAmount = drawValue;
		}
		
        drawObj.splice(drawID,1,obj);
        
        //validations
        var loanInfo = component.get("v.loanInfo");
		var repayDay = loanInfo.Repayment_Schedule_Day__c;
		var loanClose = new Date(loanInfo.Loan_Close_Date__c);
		var todayDate = new Date();
		obj.msg = "";

		
		var getDdate = obj.drawDate;
		if(getDdate){
			var ddate = new Date(getDdate.substring(5,7)+"-"+getDdate.substring(8,10)+"-"+getDdate.substring(0,4));
			var dday = ddate.getDate();
		}
		obj.msg = "";
		
		if(repayDay=="1st of Month" && dday != 1 && dday != 15){
			obj.msg = "ERROR: draw must be on the 1st or 15th of the month";
		}
		else if(repayDay=="15th of Month" && dday != 15){
			obj.msg = "ERROR: draw must be on the 15th of the month";
		}
		            
		if(ddate < loanClose){
			if(obj.msg==""){
				obj.msg = "ERROR: Draw must be after Loan Closing Date";
		        }
		        else{obj.msg = obj.msg + " / " + "ERROR: Draw must be after Loan Closing Date";}
		}
		            
		if(obj.anticipatedAmount > 0 && obj.executedAmount > 0){
			console.log('obj.msg: '+obj.msg);
			if(obj.msg==""){
				obj.msg = "ERROR: Date cannot have anticipated and executed amounts";
			}
			else{obj.msg = obj.msg + " / " + "ERROR: Date cannot have anticipated and executed amounts";}
		}
		            
		if(obj.anticipatedAmount > 0 && ddate <= todayDate){
			if(obj.msg==""){
				obj.msg = "ERROR: Anticipated Draw must be in the future";
			}
		        else{obj.msg = obj.msg + " / " + "ERROR: Anticipated Draw must be in the future";}
		}
		            
		if(obj.executedAmount > 0 && ddate > todayDate){
			if(obj.msg==""){
		     		obj.msg = "ERROR: Executed Draw can't  be in the future";
		        }
		        else{obj.msg = obj.msg + " / " + "ERROR: Executed Draw can't  be in the future";}
		}
        
        component.set("v.draws",drawObj);
        
		var totalDraws = 0;       
        var draws = component.get("v.draws");
        for(var i = 0; i < draws.length; i++){
				            totalDraws = totalDraws + parseFloat(draws[i].anticipatedAmount);
				            totalDraws = totalDraws + parseFloat(draws[i].executedAmount);
            		}
      component.set("v.totalDraws", totalDraws);
        
		$A.get('e.force:refreshView').fire();
	},
	
	repaySchedMonthSelect : function(component, event) {
		console.log('in helper.repaySchedMonthSelect');
		
		var selected = component.find("repaySchedMonthSelect").get("v.value");
		component.set("v.repaymentSchedMonthLabel", selected);

		var peMonth1;
		var peMonth2;
		var rmMonth1;
		var rmMonth2;
		
		switch (selected) {
			case "January-July":
					peMonth1 = 6;
					peMonth2 = 12;
					rmMonth1 = 1;
					rmMonth2 = 7;
					break;
					
			case "February-August":
					peMonth1 = 1;
					peMonth2 = 7;
					rmMonth1 = 2;
					rmMonth2 = 8;
					break;
			
			case "March-September":
					peMonth1 = 2;
					peMonth2 = 8;
					rmMonth1 = 3;
					rmMonth2 = 9;
					break;
					
			case "April-October":
					peMonth1 = 3;
					peMonth2 = 9;
					rmMonth1 = 4;
					rmMonth2 = 10;
					break;
					
			case "May-November":
					peMonth1 = 4;
					peMonth2 = 10;
					rmMonth1 = 5;
					rmMonth2 = 11;
					break;
					
			case "June-December":
					peMonth1 = 5;
					peMonth2 = 11;
					rmMonth1 = 6;
					rmMonth2 = 12;
					break;				
		}
		component.set("v.repaymentSchedPEmonth1", peMonth1); // "Repayment Period End Month 1" on amort tool spreadsheet (Reference Tables tab)
		component.set("v.repaymentSchedPEmonth2", peMonth2); // "Repayment Period End Month 2" on amort tool spreadsheet (Reference Tables tab)
		component.set("v.repaymentSchedRMmonth1", rmMonth1); // "Repayment Month 1" on amort tool spreadsheet (Reference Tables tab)
		component.set("v.repaymentSchedRMmonth2", rmMonth2); // "Repayment Month 2" on amort tool spreadsheet (Reference Tables tab)
		
		this.createLoanSculptingTable(component);
	},
	
	repaySchedMonthSelectNull : function(component, event) {
		console.log('in helper.repaySchedMonthSelectNull');
		
		var selected = component.find("repaySchedMonthSelectNull").get("v.value");
		component.set("v.repaymentSchedMonthLabel", selected);

		var peMonth1;
		var peMonth2;
		var rmMonth1;
		var rmMonth2;
		
		switch (selected) {
			case "January-July":
					peMonth1 = 6;
					peMonth2 = 12;
					rmMonth1 = 1;
					rmMonth2 = 7;
					break;
					
			case "February-August":
					peMonth1 = 1;
					peMonth2 = 7;
					rmMonth1 = 2;
					rmMonth2 = 8;
					break;
			
			case "March-September":
					peMonth1 = 2;
					peMonth2 = 8;
					rmMonth1 = 3;
					rmMonth2 = 9;
					break;
					
			case "April-October":
					peMonth1 = 3;
					peMonth2 = 9;
					rmMonth1 = 4;
					rmMonth2 = 10;
					break;
					
			case "May-November":
					peMonth1 = 4;
					peMonth2 = 10;
					rmMonth1 = 5;
					rmMonth2 = 11;
					break;
					
			case "June-December":
					peMonth1 = 5;
					peMonth2 = 11;
					rmMonth1 = 6;
					rmMonth2 = 12;
					break;				
		}
		component.set("v.repaymentSchedPEmonth1", peMonth1); // "Repayment Period End Month 1" on amort tool spreadsheet (Reference Tables tab)
		component.set("v.repaymentSchedPEmonth2", peMonth2); // "Repayment Period End Month 2" on amort tool spreadsheet (Reference Tables tab)
		component.set("v.repaymentSchedRMmonth1", rmMonth1); // "Repayment Month 1" on amort tool spreadsheet (Reference Tables tab)
		component.set("v.repaymentSchedRMmonth2", rmMonth2); // "Repayment Month 2" on amort tool spreadsheet (Reference Tables tab)
		
		this.createLoanSculptingTable(component);
	},
	
/* no longer needed	
	repaySchedDaySelect : function(component, event) {
		console.log('in helper.repaySchedDaySelect');
		
		var selected = component.find("repaySchedDaySelect").get("v.value");
		component.set("v.repaymentSchedDayLabel", selected);
		
		var loanInfo = component.get("v.loanInfo");
		loanInfo.Repayment_Schedule_Day__c = selected;
		component.set("v.loanInfo", loanInfo);
		
		this.createLoanSculptingTable(component);
	},
	
	repaySchedDaySelectNull : function(component, event) {
		console.log('in helper.repaySchedDaySelectNull');
		
		var selected = component.find("repaySchedDaySelectNull").get("v.value");
		component.set("v.repaymentSchedDayLabel", selected);
		
		this.createLoanSculptingTable(component);
	},
*/	
	filePurposeSelect : function(component, event) {
		console.log('in helper.filePurposeSelect');
		
		var selected = component.find("filePurposeSelect").get("v.value");
		component.set("v.filePurpose", selected);
	},
	
	handleRecordUpdated : function(component, event) {
		console.log('in helper.handleRecordUpdated');
		
		var lineID = event.getSource().get("v.name");
        console.log('lineID: '+lineID);
        var lineValue = event.getSource().get("v.value");
        console.log('lineValue: '+lineValue);
        
   		var loanInfo = component.get("v.loanInfo");
        loanInfo.WIFIA_Interest_Rate__c = lineValue;
        
        component.set("v.loanInfo", loanInfo);
   		
	},
	
	createLoanSculptingTable : function(component) {
		console.log('in helper.createLoanSculptingTable');
		
		var loan = component.get("v.loanInfo");
		
		var prevSculpt = [];
		prevSculpt = component.get("v.sculptingItems");
		if(prevSculpt.length > 0){console.log('prevSculpt[0]: '+prevSculpt[0].periodRepaymentType);}
		
		var closingDate = new Date($A.localizationService.formatDate(loan.Loan_Close_Date__c, "yyyy MM dd"));
		var dayOfClosing = closingDate.getDate();
		var monthOfClosing = closingDate.getMonth() + 1;
		var yearOfClosing = closingDate.getFullYear();
		var RepaymentScheduleDay = component.get("v.repaymentSchedDayLabel");
		var FinalMaturityDate = loan.Final_Maturity_Date__c;
		var RepaymentMonth1 = component.get("v.repaymentSchedRMmonth1");
		var RepaymentMonth2 = component.get("v.repaymentSchedRMmonth2");
		var RepaymentPeriodEndMonth1 = component.get("v.repaymentSchedPEmonth1");
		var RepaymentPeriodEndMonth2 = component.get("v.repaymentSchedPEmonth2");
		var EndOfPeriodMonth;
		var EndOfPeriodMonth2;
		var EndOfPeriodYear;
		var EndOfPeriodYear2;
		var EndOfPeriodDay;
		var EndOfPeriodDay2;
		var EndOfPeriodDayDateString;
		var EndOfPeriodDayDateString2;
		var EndOfPeriodDayTempDate;
		var EndOfPeriodDayTempDate2;
		var tempString;
		var tempString2;
		var result;
		var result2;
		
		if(RepaymentScheduleDay == '1st of Month') {
			if((monthOfClosing <= RepaymentPeriodEndMonth1) || (monthOfClosing > RepaymentPeriodEndMonth2)){
				EndOfPeriodMonth = RepaymentPeriodEndMonth1;
				EndOfPeriodMonth2 = RepaymentPeriodEndMonth2; 
			}
			else {
				EndOfPeriodMonth = RepaymentPeriodEndMonth2; 
				EndOfPeriodMonth2 = RepaymentPeriodEndMonth1;
			}
		}
		else {
			if((monthOfClosing <= RepaymentMonth1) || (monthOfClosing > RepaymentMonth2)){
				EndOfPeriodMonth = RepaymentMonth1;
				EndOfPeriodMonth2 = RepaymentMonth2;
			}
			else {
				EndOfPeriodMonth = RepaymentMonth2;
				EndOfPeriodMonth2 = RepaymentMonth1; 
			}
		}
		
		if(EndOfPeriodMonth >= monthOfClosing){
			EndOfPeriodYear = yearOfClosing;
		}
		else {
			EndOfPeriodYear = yearOfClosing + 1;
		}
		
		if(EndOfPeriodMonth2 >= monthOfClosing){
			EndOfPeriodYear2 = yearOfClosing;
		}
		else {
			EndOfPeriodYear2 = yearOfClosing + 1;
		}
		console.log('EndOfPeriodMonth: '+EndOfPeriodMonth);
		console.log('EndOfPeriodMonth2: '+EndOfPeriodMonth2);

		var peString;
		var loanSculpting = [];
		var loopMonth;
		var loopDay;
		var loopYear;
		var loopCounter = 0;
		var dateCheck;
		var rt = component.get("v.repaymentTypes");
		var today = new Date();
		var randID = String(today.getDate())+String(today.getMinutes())+String(today.getSeconds())+String(i);
			
		for(var i = 0; i < 200; i++){
			var obj = new Object();
			obj.message = "";
			if (i % 2 == 0){
				//i is even number
				loopMonth = EndOfPeriodMonth;
				loopYear = EndOfPeriodYear + loopCounter;
			
				if(RepaymentScheduleDay == '1st of Month') {
					EndOfPeriodDayDateString = loopYear+"-"+loopMonth+"-1";
					EndOfPeriodDayTempDate = new Date($A.localizationService.formatDate(EndOfPeriodDayDateString, "yyyy MM dd"));
					EndOfPeriodDayTempDate.setMonth(EndOfPeriodDayTempDate.getMonth() + 1); 
					tempString = EndOfPeriodDayTempDate.getFullYear()+"-"+(EndOfPeriodDayTempDate.getMonth()+1)+"-"+EndOfPeriodDayTempDate.getDate();
					result = new Date($A.localizationService.formatDate(tempString, "yyyy MM dd"));
					result.setDate(result.getDate() - 1);				
					EndOfPeriodDayTempDate = new Date($A.localizationService.formatDate(result, "yyyy MM dd"));			
					loopDay = EndOfPeriodDayTempDate.getDate();
				}
				else {
					loopDay = 14;
					EndOfPeriodDayDateString = loopYear+"-"+loopMonth+"-1";
					EndOfPeriodDayTempDate = new Date($A.localizationService.formatDate(EndOfPeriodDayDateString, "yyyy MM dd"));
				}
			}
				
			if (i % 2 == 1){
				//i is odd number
				loopMonth = EndOfPeriodMonth2;
				loopYear = EndOfPeriodYear2 + loopCounter;
				loopCounter++;
				if(RepaymentScheduleDay == '1st of Month') {
					EndOfPeriodDayDateString = loopYear+"-"+loopMonth+"-1";
					EndOfPeriodDayTempDate = new Date($A.localizationService.formatDate(EndOfPeriodDayDateString, "yyyy MM dd"));
					EndOfPeriodDayTempDate.setMonth(EndOfPeriodDayTempDate.getMonth() + 1); 
					tempString = EndOfPeriodDayTempDate.getFullYear()+"-"+(EndOfPeriodDayTempDate.getMonth()+1)+"-"+EndOfPeriodDayTempDate.getDate();
					result = new Date($A.localizationService.formatDate(tempString, "yyyy MM dd"));
					result.setDate(result.getDate() - 1);				
					EndOfPeriodDayTempDate = new Date($A.localizationService.formatDate(result, "yyyy MM dd"));			
					loopDay = EndOfPeriodDayTempDate.getDate();
				}
				else {
					loopDay = 14;
					EndOfPeriodDayDateString = loopYear+"-"+loopMonth+"-1";
					EndOfPeriodDayTempDate = new Date($A.localizationService.formatDate(EndOfPeriodDayDateString, "yyyy MM dd"));
				}
			}

			dateCheck = new Date($A.localizationService.formatDate(FinalMaturityDate, "yyyy MM dd"));
			
			if(EndOfPeriodDayTempDate >= dateCheck){
				break;
			}
			
			//peString = loopMonth+"-"+loopDay+"-"+loopYear;
			peString = loopMonth+"/"+loopDay+"/"+loopYear;
		//	console.log(peString);
			
			obj.indexVar = randID;
			obj.periodEnd = peString;
			
			if(prevSculpt.length > 0 && i < prevSculpt.length ){
				obj.periodRepaymentType = prevSculpt[i].periodRepaymentType; 
	     	}
	     	else{
	     		obj.periodRepaymentType = rt[0]; 
	     	}
	     	
	     	if(prevSculpt.length > 0 && i < prevSculpt.length ){
	     		obj.prinDoll = prevSculpt[i].prinDoll;
	     	}
	     	else{
	     		obj.prinDoll = 0;
	     	}
	     	
	     	if(prevSculpt.length > 0 && i < prevSculpt.length ){
	     		obj.prinPerc = prevSculpt[i].prinPerc;
	     	}
	     	else{
	     		obj.prinPerc = 0;
	     	}
	     	
	     	if(prevSculpt.length > 0 && i < prevSculpt.length ){
	     		obj.prinPerc = prevSculpt[i].intDoll;
	     	}
	     	else{
	     		obj.intDoll = 0;
	     	}
		
			if(prevSculpt.length > 0 && i < prevSculpt.length ){
	     		obj.prinPerc = intPerc[i].intDoll;
	     	}
	     	else{
	     		obj.intPerc = 0;
	     	}
			
			//validations
			if(loanSculpting.length > 0){
				if(obj.periodRepaymentType==="Level Payment (annual p, semiannual i)" && loanSculpting[i-1].periodRepaymentType!="Interest Only Total"){
					obj.message = "Error: Level Payment (annual p, semiannual i) must be preceded by an interest only payment to guarantee accuracy."
				}
				if(obj.periodRepaymentType==="Level Payment (one payment per year)" && loanSculpting[i-1].periodRepaymentType!="No Payment"){
					obj.message = "Error: Level Payment (one payment per year)) must be preceded by a no payment to guarantee accuracy.";
				}
			}
	     	
	        loanSculpting.push(obj);
	        
		}
		
		component.set("v.sculptingItems",loanSculpting);
		this.loadSculptItems(component);
	},
	
	handleRepayTypeChange : function(component, event) {
		console.log('in helper.handleRepayTypeChange');
		
		var sculptValue = event.getSource().get("v.value");
		
		var sculptID = event.getSource().get("v.name");
  		
		var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		
		var repayTypes = component.get("v.repaymentTypes");
		if(repayTypes.indexOf(sculptValue) < 0){
			var msg = " " + sculptValue + " is not a valid option.";
		}

		var obj = new Object();
		obj.indexVar = sculptObj[sculptID].indexVar;
        obj.periodEnd = sculptObj[sculptID].periodEnd;
        obj.periodRepaymentType = sculptValue;
        obj.prinDoll = sculptObj[sculptID].prinDoll;
		obj.prinPerc = sculptObj[sculptID].prinPerc;
		obj.intDoll = sculptObj[sculptID].intDoll;
		obj.intPerc = sculptObj[sculptID].intPerc;
    	obj.message = msg;
    	
    	//validations
		if(sculptID > 0){
			if(obj.periodRepaymentType==="Level Payment (annual p, semiannual i)" && sculptObj[sculptID-1].periodRepaymentType!="Interest Only Total"){
				obj.message = "Error: Level Payment (annual p, semiannual i) must be preceded by an interest only payment to guarantee accuracy."
			}
			if(obj.periodRepaymentType==="Level Payment (one payment per year)" && sculptObj[sculptID-1].periodRepaymentType!="No Payment"){
				obj.message = "Error: Level Payment (one payment per year)) must be preceded by a no payment to guarantee accuracy.";
			}
		}
    	
    	sculptObj[sculptID] = obj;
    	component.set("v.sculptingItems",sculptObj);
		//$A.get('e.force:refreshView').fire();
		
	},
	
	handlePaymentVarChange : function(component, event) {
		console.log('in helper.handlePaymentVarChange');
		
		var sculptID = event.getSource().get("v.name");
		var payVarValue = event.getSource().get("v.value");
		var payVarField = event.getSource().get("v.id");
		
		if(payVarValue.length < 1){
				payVarValue = "0";
			}
		
		console.log('sculptID: '+sculptID);
		console.log('payVarValue: '+payVarValue);
		console.log('payVarField: '+payVarField);
		
		var msg = "";
		
		var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		
		var obj = new Object();
		obj.indexVar = sculptObj[sculptID].indexVar;
        obj.periodEnd = sculptObj[sculptID].periodEnd;
        obj.periodRepaymentType = sculptObj[sculptID].periodRepaymentType;
        
        if(payVarField == "prinDoll"){
        	obj.prinDoll = payVarValue;
        	if(obj.periodRepaymentType != "Fixed Principal ($)"){
        		msg = "Error: Principal ($) should only be used for Repayment Type of Fixed Principal ($)";
        	}
        }
        else{
        	obj.prinDoll = sculptObj[sculptID].prinDoll;
        }
        
		if(payVarField == "prinPerc"){
        	obj.prinPerc = payVarValue;
        }
        else{
        	obj.prinPerc = sculptObj[sculptID].prinPerc;
        }
		
		if(payVarField == "intDoll"){
        	obj.intDoll = payVarValue;
        }
        else{
        	obj.intDoll = sculptObj[sculptID].intDoll;
        }

		if(payVarField == "intPerc"){
        	obj.intPerc = payVarValue;
        }
        else{
        	obj.intPerc = sculptObj[sculptID].intPerc;
        }
 		
 		if(obj.message){
    		msg = obj.message + msg;
    	}
    	
    	obj.message = msg;
    	
    	sculptObj[sculptID] = obj;
    	component.set("v.sculptingItems",sculptObj);
		
		
		
	},

	calculate : function(component) {
		console.log('in helper.calculate');
		// call amort tool apex /////////
		
		var loanID = component.get("v.recordId");
		var loanSculpting = component.get("v.sculptingItems");	
		console.log('sculpt items: '+loanSculpting);
		var drawItems = component.get("v.draws");
		var loanInfo = component.get("v.loanInfo");
		console.log(JSON.stringify(loanInfo));
		console.log(JSON.stringify(drawItems));
		
		var purpose = component.get("v.filePurpose");
		
		var action = component.get("c.amortCalc");
	    action.setParams({
	    	"loanID" : loanID,
	        "sculptString" : JSON.stringify(loanSculpting),
	        "drawString" : JSON.stringify(drawItems),
	        "intRate" : loanInfo.WIFIA_Interest_Rate__c,
	        "closeDate" : loanInfo.Loan_Close_Date__c,
	        "purpose" : purpose
	    });
	    action.setCallback(this, function(response){
	        var state = response.getState();
	        console.log("test state: "+state);
	        if (state === "SUCCESS") {
	        	var stringList = [];
	        	stringList = response.getReturnValue();
	        
	        	component.set("v.output1", JSON.parse(stringList[0]));
	        	//component.set("v.output2", JSON.parse(stringList[1]));
	        	
	        	var max = parseFloat(stringList[1]);
	        	component.set("v.maxOE", max);
	        	
	        	var rips = parseFloat(stringList[2]);
	        	component.set("v.sumRIP", rips);
	        	
	        	var waly = parseFloat(stringList[3]);
	        	component.set("v.walYears", waly);
	        	
	        	var walym = stringList[4];
	        	component.set("v.walYearsMonths", walym);
	        	
	        	var numPs = parseInt(stringList[5]);
	        	component.set("v.numPeriods", numPs);
	        	
	        	var numYs = numPs / 2;
	        	component.set("v.numYears", numYs);
	        	
	        	// handle validation errors
	        	console.log('stringList[6]: '+stringList[6]);
	        	if(stringList[6]==="error"){
	        		console.log('validation error found');
	        		component.set("v.calcValidation", true);
	        		
	        		var validationList = JSON.parse(stringList[7]);
	        		var obj = new Object();
	        		var sculptObj = [];
	        		for(var i = 0; i < loanSculpting.length; i++){
	        			obj = loanSculpting[i];
						obj.message = validationList[i];
						sculptObj.push(obj);
	        		}
	        		component.set("v.sculptingItems", sculptObj);	
	        	}
	     		
	     		var firstL = component.get("v.firstLoad");
	     		if(firstL){
	     			component.set("v.firstLoad", false);
	     			component.set("v.outputModal", false);
	     		}
	     		else{
	     			component.set("v.outputModal", true);
				}
					     		
	        }
	        else if(state === 'ERROR'){
	        	var errors = response.getError();
	        	for(var i = 0; i < errors.length;i++){
	            	console.log(errors[i].message);
	            	console.log(errors[i]);
	        	}
	        }
	    });
	    $A.enqueueAction(action);  
	},
	
	closeOutputModal : function(component) {
		console.log('in helper.closeOutputModal');
		
		component.set("v.outputModal", false);
	},
	
	openExhibitF : function(component) {
		console.log('in helper.openExhibitF');
		
		component.set("v.outputModal", false);
		
		var loanID = component.get("v.recordId");
		var walYM = component.get("v.walYearsMonths");
		
		var urlAndParam = "/apex/WIFIA_AmortOutput_VF?loanID="+loanID+"&walYM="+walYM;
		window.open(urlAndParam,'_blank');
		/*
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": urlAndParam,
          "isredirect": "true"
        });
        urlEvent.fire();
        */
	},

	saveAll : function(component) {
		console.log('in helper.saveAll');
		
		var draws = component.get("v.draws");
		var loanSculpting = component.get("v.sculptingItems");
		
		var sculptsWrapped = JSON.stringify(loanSculpting);
		var drawsWrapped = JSON.stringify(draws);

		var loanID = component.get("v.recordId");
		var action = component.get("c.saveJson");
        action.setParams({
            "loanID" : loanID,
            "sculptRecords" : sculptsWrapped,
            "drawRecords" : drawsWrapped
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
            	//var record = event.getParam("response");
		    	component.find("notifLib").showToast({
		        "title": "Saved",
		        "message": "Amort Data Saved",
		        });
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   

	},
	
	loadSculptItems : function(component) {
		console.log('in helper.loadSculptItems');

		var loanID = component.get("v.recordId");
		var action = component.get("c.getLoanSculptingJSON");
        action.setParams({
            "loanID" : loanID
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
            	if(response.getReturnValue() != null){
	            	//component.set("v.sculptingItems", JSON.parse(response.getReturnValue()));
	            	var loanSculpting = JSON.parse(response.getReturnValue());
	            	
	            	//validations
	            	for(var i = 0; i < loanSculpting.length; i++){
	            		loanSculpting[i].message = "";
						if(loanSculpting.length > 0){
							if(loanSculpting[i].periodRepaymentType==="Level Payment (annual p, semiannual i)" && loanSculpting[i-1].periodRepaymentType!="Interest Only Total"){
								loanSculpting[i].message = "Error: Level Payment (annual p, semiannual i) must be preceded by an interest only payment to guarantee accuracy."
							}
							if(loanSculpting[i].periodRepaymentType==="Level Payment (one payment per year)" && loanSculpting[i-1].periodRepaymentType!="No Payment"){
								loanSculpting[i].message = "Error: Level Payment (one payment per year)) must be preceded by a no payment to guarantee accuracy.";
							}
						}
					}
	            	component.set("v.sculptingItems", loanSculpting);
            	}
            	this.loadDrawItems(component);
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   

	},
	
	loadDrawItems : function(component) {
		console.log('in helper.loadDrawItems');

		var loanID = component.get("v.recordId");
		var totalDraws = 0;
		
		var action = component.get("c.getLoanDrawJSON");
        action.setParams({
            "loanID" : loanID
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
            	if(response.getReturnValue() != null){
            		var anticDraws = response.getReturnValue();
            		var execDraws = component.get("v.draws");
            		if(execDraws.length > 0){
            			var anticDraws = JSON.parse(response.getReturnValue());
            			var allDraws = [];
            			for(var i = 0; i < execDraws.length; i++){
				            allDraws.push(execDraws[i]);
            			}
            			for(var i = 0; i < anticDraws.length; i++){
				            allDraws.push(anticDraws[i]);
            			}
        			
            			component.set("v.draws", allDraws);            			
            		}
            		else{
            			component.set("v.draws", JSON.parse(response.getReturnValue()));
            		}
            		
            		var draws = component.get("v.draws");
            		for(var i = 0; i < draws.length; i++){
				            totalDraws = totalDraws + parseFloat(draws[i].anticipatedAmount);
				            totalDraws = totalDraws + parseFloat(draws[i].executedAmount);
            		}
            		component.set("v.totalDraws", totalDraws);
            		var firstL = component.get("v.firstLoad");
            		if(firstL){
	            		this.calculate(component);
	            	}
            	}
            	
            	//validations
		        var loanInfo = component.get("v.loanInfo");
				var repayDay = loanInfo.Repayment_Schedule_Day__c;
				var loanClose = new Date(loanInfo.Loan_Close_Date__c);
				var todayDate = new Date();
				
				var allDraws = component.get("v.draws");
				for(var i = 0; i < allDraws.length; i++){
					var obj = new Object();
					obj = allDraws[i];
					var getDdate = obj.drawDate;
					if(getDdate){
						var ddate = new Date(getDdate.substring(5,7)+"-"+getDdate.substring(8,10)+"-"+getDdate.substring(0,4));
						var dday = ddate.getDate();
					}
					obj.msg = "";
					
					if(repayDay=="1st of Month" && dday != 1 && dday != 15){
						obj.msg = "ERROR: draw must be on the 1st or 15th of the month";
					}
					else if(repayDay=="15th of Month" && dday != 15){
						obj.msg = "ERROR: draw must be on the 15th of the month";
					}
					            
					if(ddate < loanClose){
						if(obj.msg==""){
							obj.msg = "ERROR: Draw must be after Loan Closing Date";
					        }
					        else{obj.msg = obj.msg + " / " + "ERROR: Draw must be after Loan Closing Date";}
					}
					            
					if(obj.anticipatedAmount > 0 && obj.executedAmount > 0){
						if(obj.msg==""){
							obj.msg = "ERROR: Date cannot have anticipated and executed amounts";
						}
						else{obj.msg = obj.msg + " / " + "ERROR: Date cannot have anticipated and executed amounts";}
					}
					            
					if(obj.anticipatedAmount > 0 && ddate <= todayDate){
						if(obj.msg==""){
							obj.msg = "ERROR: Anticipated Draw must be in the future";
						}
					        else{obj.msg = obj.msg + " / " + "ERROR: Anticipated Draw must be in the future";}
					}
					            
					if(obj.executedAmount > 0 && ddate > todayDate){
						if(obj.msg==""){
					     		obj.msg = "ERROR: Executed Draw can't  be in the future";
					        }
					        else{obj.msg = obj.msg + " / " + "ERROR: Executed Draw can't  be in the future";}
					}
            	}
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   

	},
	
	getVals: function(component, event) {
        console.log('in helper.getVals');
        
        var sculptItems = component.get("v.sculptingItems");
        var sculptID = event.getSource().get("v.name");
        var acceptedValues = component.get("v.repaymentTypes");
        const searchString = event.getSource().get("v.value");
        var predictions = [];
        if (searchString.length >= 3) {
            console.log(searchString);
			for (var i = 0; i < acceptedValues.length; i++) {
                console.log(acceptedValues[i]);
           		if (acceptedValues[i].startsWith(searchString)) predictions.push(acceptedValues[i]);
            }            
        }
        //console.log(predictions);
        component.set('v.predictItem', sculptItems[sculptID].indexVar);
        component.set('v.predictions',predictions);
        
        var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		sculptObj[sculptID].periodRepaymentType = searchString;
		sculptObj[sculptID].message = '';
		//validations

		if(acceptedValues.indexOf(sculptObj[sculptID].periodRepaymentType) < 0){
			sculptObj[sculptID].message = sculptObj[sculptID].periodRepaymentType + " is not a valid option.";
		}
		if(sculptID > 0){
			if(sculptObj[sculptID].periodRepaymentType==="Level Payment (annual p, semiannual i)" && sculptObj[sculptID-1].periodRepaymentType!="Interest Only Total"){
				sculptObj[sculptID].message = "Error: Level Payment (annual p, semiannual i) must be preceded by an interest only payment to guarantee accuracy."
			}
			if(sculptObj[sculptID].periodRepaymentType==="Level Payment (one payment per year)" && sculptObj[sculptID-1].periodRepaymentType!="No Payment"){
				sculptObj[sculptID].message = "Error: Level Payment (one payment per year)) must be preceded by a no payment to guarantee accuracy.";
			}
		}
		
		component.set("v.sculptingItems", sculptObj);
    },
    
    getDetails: function(component, event) {
    	console.log('in helper.getDetails');
    	var sculptID = event.getSource().get("v.name");
    	//var sculptID = event.target.name;
        console.log('sculptID: '+sculptID);
        //var selectedItem = event.target.id;
        var selectedItem = event.getSource().get("v.label");
        console.log('selectedItem: '+selectedItem);
        var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		sculptObj[sculptID].periodRepaymentType = selectedItem;
		sculptObj[sculptID].message = '';
		//validations
		var repayTypes = component.get("v.repaymentTypes");
		if(repayTypes.indexOf(sculptObj[sculptID].periodRepaymentType) < 0){
			sculptObj[sculptID].message = sculptObj[sculptID].periodRepaymentType + " is not a valid option.";
		}
		if(sculptID > 0){
			if(sculptObj[sculptID].periodRepaymentType==="Level Payment (annual p, semiannual i)" && sculptObj[sculptID-1].periodRepaymentType!="Interest Only Total"){
				sculptObj[sculptID].message = "Error: Level Payment (annual p, semiannual i) must be preceded by an interest only payment to guarantee accuracy."
			}
			if(sculptObj[sculptID].periodRepaymentType==="Level Payment (one payment per year)" && sculptObj[sculptID-1].periodRepaymentType!="No Payment"){
				sculptObj[sculptID].message = "Error: Level Payment (one payment per year)) must be preceded by a no payment to guarantee accuracy.";
			}
		}
		
		component.set("v.sculptingItems", sculptObj);
		component.set("v.predictions","");
		component.set("v.predictItem","");
    },

////////////////////////////////////////////////////////////////////////////////////////////////////////	
// below code was built incorrectly for creating sculpting table manually, but may have use later... ///
////////////////////////////////////////////////////////////////////////////////////////////////////////

	deleteSculptItem : function(component, event) {
		console.log('in helper.deleteSculptItem');

		var sculptID = event.getSource().get("v.name");
		
		var sculptObj = [];
		sculptObj = component.get("v.draws");
		sculptObj.splice(sculptID,1);

		component.set("v.sculptingItems",sculptObj);
		$A.get('e.force:refreshView').fire();
	},
	
	addSculptItem : function(component, event) {
		console.log('in helper.addSculptItem');
		
		var today = new Date();
		var randID = String(today.getDate())+String(today.getMinutes())+String(today.getSeconds());
		var rt = component.get("v.repaymentTypes");
		
		var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		
		var obj = new Object();
		obj.id = randID;
        obj.periodEnd;
        obj.periodRepaymentType = rt[0];
        sculptObj.push(obj);
        
        console.log('on create, obj id: '+sculptObj[0].id);
        
        component.set("v.sculptingItems",sculptObj);
        component.set("v.testItem", sculptObj);
    //    component.find(sculptObj[0].id).set("v.value", rt[2].index);
	//	$A.get('e.force:refreshView').fire();
	},
	
	saveEndDate : function(component, event) {
		console.log('in helper.saveEndDate');
			
	 	var sculptValue = event.getSource().get("v.value");
		
		var sculptID = event.getSource().get("v.name");
  		console.log('sculptID: '+sculptID);
		console.log('sculptValue: '+sculptValue);
		
		var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		
		var obj = new Object();
		obj.id = sculptObj[0].id;
        obj.periodEnd = sculptValue;
        obj.periodRepaymentType = sculptObj[sculptID].periodRepaymentType;
        sculptObj.splice(sculptID,1,obj);
        
        component.set("v.sculptingItems",sculptObj);
		$A.get('e.force:refreshView').fire();
	},
	
	editSculptItem : function(component, event) {
		console.log('in helper.editSculptItem');
		
		component.set("v.isModalOpen", true);
		
		var sculptID = event.getSource().get("v.name");
		component.set("v.editingIndex", sculptID);
		console.log('sculptID: '+sculptID);
		
		var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		console.log('obj id: '+sculptObj[sculptID].id);
		var currentRT = sculptObj[sculptID].periodRepaymentType.index;
		console.log('current repaytype: '+sculptObj[sculptID].periodRepaymentType.description);
		
		var repayTypes = component.get("v.repaymentTypes");
		
		var opts = [];
		var opt;
		for(var i = 0; i < repayTypes.length; i++){
			if(currentRT==repayTypes[i].index){
				opt = {"class": "optionClass", label: repayTypes[i].description, value: repayTypes[i].index, selected: "true" }
			}
			else {
				opt = {"class": "optionClass", label: repayTypes[i].description, value: repayTypes[i].index }
			}
			opts.push(opt);
		}
		
		component.find("peType").set("v.options", opts);
		
		var sculptItems = component.get("v.sculptingItems");

		var sculptItem = sculptItems[sculptID];
		component.set("v.periodEnd", sculptItem.periodEnd);
		console.log('sculptItem.periodEnd: '+sculptItem.periodEnd);
		
		component.set("v.repaymentType", sculptItem.periodRepaymentType);
		console.log('sculptItem.periodRepaymentType.index: '+sculptItem.periodRepaymentType.index);
		console.log('sculptItem.periodRepaymentType.value: '+sculptItem.periodRepaymentType.value);
		console.log('sculptItem.periodRepaymentType.description: '+sculptItem.periodRepaymentType.description);
		
	},
	
	saveSculptChanges : function(component, event) {
		console.log('in helper.saveSculptChanges');
		
		var peDate = component.find("peDate").get("v.value");
		console.log('peDate: '+peDate);
		
//		var peType = component.find("peType").get("v.value");
//		console.log('peType: '+peType);
		
		var sculptID = component.get("v.editingIndex");
		
		var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		console.log('obj id: '+sculptObj[sculptID].id);
		
		sculptObj[sculptID].periodEnd = peDate;
//		sculptObj[sculptID].periodRepaymentType = peType;
		
	    component.set("v.sculptingItems", sculptObj);
	    component.set("v.periodEnd", null);
	    component.set("v.repaymentType", null);
	    component.set("v.isModalOpen", false);
	},
	
	saveRepaymentType : function(component, event) {
		console.log('in helper.saveRepaymentType');
		
		var rt = component.get("v.repaymentTypes");
		
//		var peT = component.get("v.periodRepaymentType");
//		console.log('peT: '+peT);
		
//		var selected = component.find("peType").get("v.value");
//		console.log('selected: '+selected);

		var	sculptValue = event.getSource().get("v.value");
		console.log('sculptValue: '+sculptValue);
 		sculptValue = sculptValue-1;
		
//		var sculptID = event.getSource().get("v.name");
 		var sculptID = component.get("v.editingIndex");
 		console.log('sculptID: '+sculptID);
		
		var sculptObj = [];
		sculptObj = component.get("v.sculptingItems");
		console.log('obj id: '+sculptObj[sculptID].id);
		
	   sculptObj[sculptID].periodRepaymentType = rt[sculptValue];
	   component.set("v.sculptingItems", sculptObj);
	   
	   console.log('new update: '+sculptObj[sculptID].periodRepaymentType.description);
	//   $A.get('e.force:refreshView').fire() ;
	/*		
		var obj = new Object();
		obj.id = sculptObj[sculptID].id;
        obj.periodEnd = sculptObj[sculptID].periodEnd;
        obj.periodRepaymentType = rt[sculptValue];
        sculptObj.splice(sculptID,1,obj);
        
        console.log('obj.periodRepaymentType.index: '+obj.periodRepaymentType.index);
        console.log('obj.periodRepaymentType.value: '+obj.periodRepaymentType.value);
        console.log('obj.periodRepaymentType.description: '+obj.periodRepaymentType.description);
        
        component.set("v.sculptingItems",sculptObj);

     //   component.find(0).set("v.value", rt[sculptValue].index);
 		$A.get('e.force:refreshView').fire() ;
 	
 	*/
 	
	},
	
	cancelSculptChanges : function(component, event) {
		console.log('in helper.editSculptItem');
		
		component.set("v.isModalOpen", false);
	}

})