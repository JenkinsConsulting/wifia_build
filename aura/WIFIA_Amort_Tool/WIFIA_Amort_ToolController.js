({
	doInit : function(component, event, helper) {
        console.log('in doInit');
		
        //get disbursement data
        var loanID = component.get("v.recordId");

        var action = component.get("c.getDisbursements");
        action.setParams({
            "loanID" : loanID
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state: "+state);
            if (state === "SUCCESS") {
                component.set("v.retDisburs", response.getReturnValue());
                helper.loadLoanInfo(component);
            }
            else if(state === 'ERROR'){
            	var errors = response.getError();
            	for(var i = 0; i < errors.length;i++){
                	console.log(errors[i].message);
            	}
            }
        });
        $A.enqueueAction(action);   
     
	},
	
	deleteDraw : function(component, event, helper) {
		helper.deleteDraw(component, event);
	},
	
	addDraw : function(component, event, helper) {
		helper.addDraw(component, event);
	},
	
	saveDraw : function(component, event, helper) {
		helper.saveDraw(component, event);
	},
	
	updateDraw : function(component, event, helper) {
		helper.updateDraw(component, event);
	},
	
	cancelSaveDraw : function(component, event, helper) {
		helper.cancelSaveDraw(component, event);
	},
	
	handleOnDrawSaveSuccess  : function(component, event, helper) {
		helper.handleOnDrawSaveSuccess(component, event);
	},
	
	saveDrawDate : function(component, event, helper) {
		helper.saveDrawDate(component, event);
	},
	
	saveDrawAnticipatedAmount : function(component, event, helper) {
		helper.saveDrawAnticipatedAmount(component, event);
	},
	
	saveDrawExecutedAmount : function(component, event, helper) {
		helper.saveDrawExecutedAmount(component, event);
	},
	
	deleteSculptItem : function(component, event, helper) {
		helper.deleteSculptItem(component, event);
	},
	
	addSculptItem : function(component, event, helper) {
		helper.addSculptItem(component, event);
	},
	
	saveEndDate : function(component, event, helper) {
		helper.saveEndDate(component, event);
	},
	
	saveRepaymentType : function(component, event, helper) {
		helper.saveRepaymentType(component, event);
	},
	
	editSculptItem : function(component, event, helper) {
		helper.editSculptItem(component, event);
	},
	
	handleRepayTypeChange : function(component, event, helper) {
		helper.handleRepayTypeChange(component, event);
	},
	
	saveSculptChanges : function(component, event, helper) {
		helper.saveSculptChanges(component, event);
	},
	
	cancelSculptChanges : function(component, event, helper) {
		helper.cancelSculptChanges(component, event);
	},
	
	handleRecordUpdated : function(component, event, helper) {
        helper.handleRecordUpdated(component, event);
	},
	
	repaySchedMonthSelect : function(component, event, helper) {
		helper.repaySchedMonthSelect(component, event);
	},
	
	repaySchedMonthSelectNull : function(component, event, helper) {
		helper.repaySchedMonthSelectNull(component, event);
	},
	
	repaySchedDaySelect : function(component, event, helper) {
		helper.repaySchedDaySelect(component, event);
	},
	
	repaySchedDaySelectNull : function(component, event, helper) {
		helper.repaySchedDaySelectNull(component, event);
	},
	
	filePurposeSelect : function(component, event, helper) {
		helper.filePurposeSelect(component, event);
	},
	
	calculate : function(component, event, helper) {
		helper.calculate(component);
	},
	
	saveAll : function(component, event, helper) {
		helper.saveAll(component);
	},
	
	loadAll : function(component, event, helper) {
		helper.loadSculptItems(component);
	},
	
	closeOutputModal : function(component, event, helper) {
		helper.closeOutputModal(component);
	},
	
	handlePaymentVarChange : function(component, event, helper) {
		helper.handlePaymentVarChange(component, event);
	},
	
	openExhibitF : function(component, event, helper) {
		helper.openExhibitF(component, event);
	},
	
	getVals : function(component, event, helper) {
		helper.getVals(component, event);
	},
	
	getDetails: function(component, event, helper) {
		helper.getDetails(component, event);
	}
	
})